# coding: utf-8
from django.db import models
from translation.models import TranslationGetterMixin, TranslationModel
from translation import lazy_translate as _


class Person(models.Model, TranslationGetterMixin):

    user = models.OneToOneField("auth.User", related_name="person", editable=True,
                                verbose_name=u"Пользователь")
    is_staff = models.BooleanField(default=False,
                                   verbose_name=_(u"Член команды"))
    languages = models.ManyToManyField("translation.Language", blank=True,
                                       null=True, verbose_name=_(u"Языки"))
    avatar = models.ImageField(upload_to="profile/avatars/", blank=True,
                               null=True, verbose_name=_(u"Аватар"))

    class Meta:
        verbose_name = u"Персона"
        verbose_name_plural = u"Персоны"

    def __str__(self):
        return self.get_local_get_name()

    @staticmethod
    def get_absolute_url():
        return "#"


class PersonTranslation(TranslationModel):

    base_model = Person

    name = models.CharField(max_length=64, verbose_name=_(u"Имя"))
    surname = models.CharField(max_length=64, verbose_name=_(u"Фамилия"))

    description = models.TextField(verbose_name=_(u"Описание"))
    position = models.CharField(max_length=128, verbose_name=_(u"Должность"))

    class Meta:
        verbose_name = u"Перевод Персоны"
        verbose_name_plural = u"Переводы Персон"

    def get_name(self):
        return "{0} {1}".format(self.name, self.surname)

    @staticmethod
    def get_absolute_url():
        return "#"


class PersonExternalLink(models.Model):

    person = models.ForeignKey("Person", related_name=u"links")
    link = models.URLField(verbose_name=_(u"Ссылка"))

    class Meta:
        verbose_name = u"Ссылка"
        verbose_name_plural = u"Ссылки"

