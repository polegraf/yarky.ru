# environment.py
#
# Copyright (c) 2009 Trevor Caira
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Create a Jinja2 Environment object and install filters and globals.

Templates will be searched for by default in template subdirectories
of installed applications and then in directories listed in the
``TEMPLATE_DIRS`` setting.

The default environment settings can be overridden by creating a Django
setting ``JINJA2_ENVIRONMENT_OPTIONS``, which is a dictionary of
parameter to pass to `jinja2.Environment`. For example, to enable strict
undefined handling, one could set:

.. python::
    from jinja2 import StrictUndefined

    JINJA2_ENVIRONMENT_OPTIONS = {'undefined': StrictUndefined}

"""

from itertools import chain
import sys

from django.conf import settings
from django.template.utils import get_app_template_dirs
from django.utils import translation
from jinja2 import Environment, FileSystemLoader, FileSystemBytecodeCache
from jinja2.ext import i18n, with_

from chouwa import defaultglobals

from weakref import WeakKeyDictionary
from jinja2 import contextfunction
from jinja2.ext import Extension, nodes


class IfChangedExtension(Extension):
    """An extension that implements ifchanged (like in django) for Jinja2"""
    tags = set(['ifchanged'])

    _buffered_data = WeakKeyDictionary()

    def parse(self, parser):
        # get the line number of our tag and skip the "ifchanged" name
        lineno = parser.stream.next().lineno
        # create the arguments for our "_if_changed" method on our extension.
        # it only gets one constant argument that is a (not that unique, but
        # good enough for this example) key it uses to buffer the ifchanged body
        args = [nodes.Const(parser.free_identifier().name,
                            lineno=lineno)]
        # parse everything up to endifchanged and drop the "endifchange" name
        # (the needle)
        body = parser.parse_statements(['name:endifchanged'],
                                       drop_needle=True)
        # create a callblock node that calls "_if_changed" with the arguments
        # from above.  The callblock passes a function to the function as
        # "caller" keyword argument that renders the body.
        return nodes.CallBlock(self.call_method('_if_changed', args),
                               [], [], body).set_lineno(lineno)

    @contextfunction
    def _if_changed(self, context, key, caller):
        """The if-changed callback.  Because it's a contextfunction it also
        gets the context as first argument.
        """
        try:
            buffers = self._buffered_data[context]
        except KeyError:
            self._buffered_data[context] = buffers = {}
        body = caller()
        if key not in buffers or buffers[key] != body:
            rv = body
        else:
            rv = u''
        buffers[key] = body
        return rv

class DjangoTranslator(object):
    def ugettext(self, *args):
        return translation.ugettext(*args)

    def ungettext(self, *args):
        return translation.ungettext(*args)

def get_app_modules():
    """
    Generator yielding module objects containing application-specific
    jinjaglobals. Note that the globals are not namespaced for the
    application.

    """

    for app_label in settings.INSTALLED_APPS:
        mod_name = '.'.join((app_label, 'jinja2'))
        try:
            __import__(mod_name, {}, {}, [], 0)
            yield sys.modules[mod_name]
        except ImportError:
            pass

def install_globals(env):
    """
    Add the default filters and globals to the jinja2 environment.

    :Parameters:
      env : `jinja2.Environment`
        The jinja2 environment.

    """

    for mod in chain((defaultglobals,), get_app_modules()):
        for name in dir(mod):
            global_ = getattr(mod, name)
            if getattr(global_, 'is_jinja_global', False):
                env.globals[name] = global_
            elif getattr(global_, 'is_jinja_filter', False):
                env.filters[name] = global_
            elif getattr(global_, 'is_jinja_test', False):
                env.tests[name] = global_
            if name == "JINJA_GLOBAL_VARS" and type(global_) is dict:
                env.globals.update(global_)


def environment(**options):
    """
    Create and populate the jinja2 environment.

    """
    template_dirs = getattr(settings, "JINJA_TEMPLATE_DIRS", settings.TEMPLATE_DIRS)
    searchpath = get_app_template_dirs('templates') + template_dirs
    #env_options = {'loader': FileSystemLoader(searchpath),
    #               'autoescape': True,
    #               'auto_reload': settings.TEMPLATE_DEBUG}
    options.update({
        'extensions': (i18n, with_, IfChangedExtension),
    })
    options.update(getattr(settings, 'JINJA2_ENVIRONMENT_OPTIONS', {}))
    env = Environment(**options)
    install_globals(env)
    # env.install_gettext_translations(DjangoTranslator())
    env.bytecode_cache = FileSystemBytecodeCache()
    return env
