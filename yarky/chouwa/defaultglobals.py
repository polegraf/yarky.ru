from datetime import datetime
from django.contrib.staticfiles.templatetags import staticfiles

from django.core.urlresolvers import reverse
from django.utils.dateformat import format
from django.conf import settings

from django.contrib.humanize.templatetags.humanize import naturalday, apnumber
from django.template.defaultfilters import \
     (addslashes, escapejs, floatformat, iriencode, linenumbers,
      slugify, truncatewords, truncatewords_html,
      urlencode, cut, rjust, ljust, linebreaks,
      linebreaksbr, removetags, unordered_list,
      time, timesince, timeuntil, yesno,
      pluralize, phone2numeric)
from user_agents import parse

from chouwa.decorators import jinja2_global, jinja2_filter
from translation import translator


for func in (addslashes, escapejs, floatformat, iriencode, linenumbers,
             slugify, truncatewords, truncatewords_html,
             urlencode, cut, rjust, ljust, linebreaks,
             linebreaksbr, removetags, unordered_list,
             time, timesince, timeuntil, yesno,
             pluralize, phone2numeric, naturalday, apnumber):

    func.is_jinja_filter = True


@jinja2_global
def url(viewname, *args, **kwargs):
    '''
    Lookup a url via `django.core.urlresolvers.reverse`. Positional and
    keyword arguments to this function are used directly as captures in
    the URL.

    Use `url` when you need to specify a prefix or urlconf for
    ``reverse`` or if you have a URL capture named ``viewname``.

    :Parameters:
      viewname : str
        The label associated with a url or the dotted path module name
        of a view.
    '''

    return reverse(viewname, args=args, kwargs=kwargs)

@jinja2_global
def local_url(viewname, *args, **kwargs):
    '''
    Lookup a url via `django.core.urlresolvers.reverse`. Positional and
    keyword arguments to this function are used directly as captures in
    the URL.

    Use `url` when you need to specify a prefix or urlconf for
    ``reverse`` or if you have a URL capture named ``viewname``.

    :Parameters:
      viewname : str
        The label associated with a url or the dotted path module name
        of a view.
    '''
    kwargs.update({"lang": translator.current_lang})
    return reverse(viewname, args=args, kwargs=kwargs)

@jinja2_global
def url2(viewname, *args, **kwargs):
    '''
    Exactly proxy the API of `django.core.urlresolvers.reverse`.

    :Parameters:
      viewname : str
        The label associated with a url or the dotted path module name
        of a view.
    '''

    return reverse(viewname, *args, **kwargs)

@jinja2_global
def now(format_string):
    '''
    Displays the date, formatted according to the given string.

    Format specifiers are identical to those understood by PHP's
    ``date()`` function. See <http://php.net/date>.

    Sample usage::

        It is {{ now("F jS, Y H:i") }}.

    Based on `django.template.defaulttags.now`.

    :Parameters:
      format_string : str
        The PHP ``date()``-style format specifier.

    '''

    return format(datetime.now(), format_string)

@jinja2_filter
def date(dt, format_string):
    '''
    Format a date using the PHP ``date()`` format specifiers. Similar to
    the `now` function, except that it formats an arbitrary date.

    :Parameters:
      dt : `datetime.datetime`
        The datetime object to format.
      format_string : str
        The PHP ``date()``-style format specifier.

    '''

    return format(dt, format_string)

@jinja2_filter
def add(value, value2):
    return value+value2

from chouwa.decorators import jinja2_global


@jinja2_global
def windows_user(request):
    user_agent = parse(request.META.get("HTTP_USER_AGENT"))
    return "windows" in user_agent.os.family.lower()


@jinja2_global
def static(path):
    return staticfiles.static(path)


JINJA_GLOBAL_VARS = {
    "STATIC_PREFIX": settings.STATIC_URL,
    "STATIC_URL": settings.STATIC_URL,
    "MEDIA_URL": settings.MEDIA_URL,
}
