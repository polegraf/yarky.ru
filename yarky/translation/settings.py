# -*- coding: utf-8 -*-
from django.conf import settings

TRANSLATIONS_DEFAULT_LANGUAGE = getattr(settings, "TRANSLATIONS_DEFAULT_LANGUAGE", settings.LANGUAGE_CODE)
TRANSLATIONS_CACHE_LIFETIME = getattr(settings, "TRANSLATIONS_CACHE_LIFETIME", 15*60)