# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from chouwa.decorators import jinja2_global, jinja2_render

from translation import translator, lazy_translate as _
from translation.models import Language


@jinja2_global
def trans(message, *args):
    return _(message, args)


@jinja2_global
def get_languages():
    return Language.objects.all()


@jinja2_global
def current_lang():
    return translator.current_lang


@jinja2_global
@jinja2_render(template="include/language_switcher.html")
def language_switcher(path):
    return {"path": path}


@jinja2_global
def localize_month_by_index(index, decline=False, short=False):
    return localize_month(datetime(2015, index, 1).date(), decline, short)

@jinja2_global
def localize_month(date, decline=False, short=False):
    if not translator.current_lang == 'ru':
        return date.strftime("%b" if short else "%B")
    month_ru = (
        u"Январь", u"Февраль", u"Март", u"Апрель", u"Май", u"Июнь",
        u"Июль", u"Август", u"Сентябрь", u"Октябрь", u"Ноябрь", u"Декабрь", )
    months_ru_short = (
        u"Янв", u"Фев", u"Мар", u"Апр", u"Май", u"Июн",
        u"Июл", u"Авг", u"Сен", u"Окт", u"Ноя", u"Дек", )
    month_ru_declined = (
        u"Января", u"Февраля", u"Марта", u"Апреля", u"Мая", u"Июня",
        u"Июля", u"Августа", u"Сентября", u"Октября", u"Ноября", u"Декабря", )
    months = month_ru
    if decline:
        months = month_ru_declined
    if short:
        months = months_ru_short
    if date is not None:
        try:
            return months[date.month-1]
        except IndexError:
            return ""
    return months


@jinja2_global
def weekdays():
    week_ru = (
        u"Понедельник", u"Вторник", u"Среда", u"Четверг", u"Пятница",
        u"Суббота", u"Воскресенье", )
    week_en = (
        "Monday", "Tuesday", "Medium", "Thursday", "Friday", "Saturday",
        "Sunday", )
    return translator.current_lang == 'ru' and week_ru or week_en

@jinja2_global
def localize_date(month, day, with_comma=False):
    return translator.localize_date(month, day, with_comma)


@jinja2_global
def localize_time(hours, minutes):
    return translator.localize_time(hours, minutes)


@jinja2_global
def translate_path(path, lang):
    return path.replace("/{0}/".format(translator.current_lang),
                        "/{0}/".format(lang))


@jinja2_global
def yesterday():
    return datetime.today()-timedelta(days=1)


@jinja2_global
def today():
    return datetime.today()


@jinja2_global
def tommorow():
    return datetime.today()+timedelta(days=1)

