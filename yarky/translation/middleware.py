from django.core.urlresolvers import resolve, Resolver404
from django.http import HttpResponseRedirect, Http404, HttpResponseNotFound
from translation.models import Language
from translation import translator


class TranslationMiddleware(object):

    def process_request(self, request):
        current_language_code = request.path.split("/")[1] or ""
        if not len(current_language_code) == 2:
            current_language_code = request.session.get("LANGUAGE_CODE", "ru")
        try:
            request.language = Language.objects.get(code=current_language_code)
        except Language.DoesNotExist:
            request.language = Language.objects.all()[0]
        request.language_code = current_language_code
        translator.set_language(current_language_code)
        if request.session.get("LANGUAGE_CODE", "ru") != current_language_code:
            request.session["LANGUAGE_CODE"] = current_language_code

    def process_response(self, request, response):
        if isinstance(response, HttpResponseNotFound):
            try:
                local_path = "/"+request.session.get("LANGUAGE_CODE", "ru")+request.path
                resolve(local_path)
                return HttpResponseRedirect(local_path)
            except Resolver404:
                pass
        return response
