# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from django.utils.encoding import force_str

from django.utils.functional import lazy

from . import settings

class Translator:

    _cache = None
    _cache_expire = None
    _current_lang = settings.TRANSLATIONS_DEFAULT_LANGUAGE

    def __init__(self, *args, **kwargs):
        pass

    def translate(self, message, args):
        """
        Try to translate requested string
        """
        self._check_cache()  # Validate cache
        result = self._cache.get(message, message)  # search
        if args:
            return force_str(result%args)  # apply args
        return force_str(result)

    def _check_cache(self):
        """
        Validates cache. If there is no cache (first request after worker start)
        or cache is expired - rebuild it
        """
        if self._cache is None or self._cache_expire is None or self._cache_expire < datetime.now():
            self._rebuid_cache()

    def _rebuid_cache(self):
        """
        Loads translations from database to worker memory.
        TODO: load all languages to prevent reloading cache while processing
            requests with different languages
        Project is not so big, to cause problems, but...
        TODO: prevent too big memory usage
        """
        from .models import Translation
        self._cache = {}
        for message in Translation.objects.filter(language__code=self._current_lang):
            self._cache[message.key] = message.value
        self._cache_expire = datetime.now()+timedelta(seconds=settings.TRANSLATIONS_CACHE_LIFETIME)

    def set_language(self, lang):
        """
        Updates current language.
        TODO: Refactor. Now reloads WORKERS cache while REQUEST processing
        """
        self._current_lang = lang
        self._cache = None

    @property
    def current_lang(self):
        return self._current_lang

    def localize_date(self, month, day, with_comma=False):
        if translator.current_lang == "en":

            return u"{0} {1}{2}".format(month.capitalize(), day, "," if with_comma else "")
        return u"{0} {1}".format(day, month.lower())


    def localize_time(self, hours, minutes):
        if translator.current_lang == "en":
            hours = int(hours)
            m = "am"
            if hours > 12:
                m = "pm"
                hours -= 12
            if minutes == "00":
                minutes = ""
            else:
                minutes = u":{0}".format(minutes)
            return u"{0}{1} {2}".format(hours, minutes, m)
        return "{0}:{1}".format(hours, minutes)

# Create singleton
translator = Translator()

del Translator #remove Translator class from namespace, to prevent new instances creation

# Make tranlation function
def translate(name, args=None):
    return translator.translate(name, args)

# And lazy version
lazy_translate = lazy(translate, str)
