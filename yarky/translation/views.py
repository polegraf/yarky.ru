from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from translation import translator


def current_lang_redirect(request):
    return HttpResponseRedirect(reverse("home", args=[translator.current_lang]))
