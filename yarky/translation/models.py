# -*- coding: utf-8 -*-
from django.db import models
from . import lazy_translate as _, translator


class Language(models.Model):
    title = models.CharField(max_length=150, verbose_name=_(u"Название"))
    code = models.CharField(max_length=2, verbose_name=_(u"Код"))

    class Meta:
        verbose_name=_(u"Language")
        verbose_name_plural = _(u"Languages")
        ordering = ["id"]

    def __str__(self):
        return self.title


class Translation(models.Model):
    language = models.ForeignKey(Language)
    key = models.TextField(unique=True)
    value = models.TextField()

    class Meta:
        verbose_name = _(u"Translation")
        verbose_name_plural = _(u"Translations")


class TranslationManager(models.Manager):

    def get_query_set(self, *args, **kwargs):
        return super(TranslationManager, self).get_query_set(
            *args, **kwargs).select_related("base", "language")


class TranslationModelBase(models.base.ModelBase):

    def __new__(cls, name, bases, attrs):
        base_model = attrs.get("base_model", None)
        if base_model:
            attrs["base"] = models.ForeignKey(
                base_model, related_name="translations", editable=False)
        return super(TranslationModelBase, cls).__new__(cls, name, bases, attrs)


class TranslationModel(models.Model, metaclass=TranslationModelBase):

    base_model = None
    language = models.ForeignKey("translation.Language",
                                 verbose_name=_("Language"))

    objects = TranslationManager()

    class Meta:
        abstract = True
        unique_together = (('base', 'language'), )

    def __init__(self, *args, **kwargs):
        super(TranslationModel, self).__init__(*args, **kwargs)


class TranslationGetterMixin(object):

    def get_local(self):
        if not hasattr(self, "_local_translation"):
            self._local_translation = self.translations.get(
                language__code=translator.current_lang
            )
        return self._local_translation

    def __getattribute__(self, name):
        if name.startswith("get_local_"):
            real_name = name.replace("get_local_", "")
            try:
                return getattr(self.get_local(), real_name)
            except Exception as ex:
                pass

        return super().__getattribute__(name)
