# coding: utf-8

from django.conf.urls import *
from blog.views import BlogView, BlogEntryView, BlogCalendarView

urlpatterns = patterns('admin.views',
    url(r'^$', BlogView.as_view(), name="list"),
    url(r'^get_calendar/$', BlogCalendarView.as_view(), name="get-calendar"),
    url(r'^(?P<slug>[a-z0-9\-\_]+)/$', BlogEntryView.as_view(), name="entry"),
)
