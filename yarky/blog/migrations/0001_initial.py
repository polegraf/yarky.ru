# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import yarky.helpers
import translation.models
import tagging.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '__first__'),
        ('translation', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogEntry',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(default='Запись блога', verbose_name='Системное название', max_length=150, editable=False)),
                ('slug', models.SlugField(verbose_name='Код для URL', max_length=150)),
                ('cover', models.ImageField(null=True, upload_to='blog/covers', blank=True)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
                ('published_date', models.DateField()),
                ('pinned', models.BooleanField(default=False, verbose_name='Прилеплена')),
                ('published', models.BooleanField(default=False, verbose_name='Опубликована')),
                ('facebook_link', models.URLField(null=True, blank=True)),
                ('twitter_link', models.URLField(null=True, blank=True)),
                ('author', models.ForeignKey(verbose_name='Автор', to='profile.Person', related_name='blog_entries')),
            ],
            options={
                'verbose_name': 'Запись блога',
                'verbose_name_plural': 'Блог',
                'ordering': ['-published_date'],
            },
            bases=(models.Model, translation.models.TranslationGetterMixin),
        ),
        migrations.CreateModel(
            name='BlogEntryTranslation',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(verbose_name='Заголовок', max_length=150)),
                ('text', yarky.helpers.HTMLField(verbose_name='Текст')),
                ('announce', models.TextField(default='', verbose_name='Лид', blank=True)),
                ('tags', tagging.fields.TagField(max_length=255, blank=True)),
                ('base', models.ForeignKey(editable=False, to='blog.BlogEntry', related_name='translations')),
                ('language', models.ForeignKey(verbose_name='Language', to='translation.Language')),
            ],
            options={
                'verbose_name': 'Запись блога',
                'verbose_name_plural': 'Блог',
                'ordering': ['-base__published_date'],
            },
        ),
    ]
