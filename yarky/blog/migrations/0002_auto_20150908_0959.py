# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogentry',
            name='slug',
            field=models.SlugField(max_length=255, verbose_name='Код для URL'),
        ),
        migrations.AlterField(
            model_name='blogentry',
            name='title',
            field=models.CharField(max_length=255, editable=False, default='Запись блога', verbose_name='Системное название'),
        ),
        migrations.AlterField(
            model_name='blogentrytranslation',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Заголовок'),
        ),
    ]
