# coding: utf-8
from datetime import datetime
from django.http import JsonResponse
from django.views.generic import ListView, DetailView, View
from tagging.models import TaggedItem
from blog.jinja2 import blog_calendar
from blog.models import BlogEntryTranslation
from translation import translator


class BlogView(ListView):

    template_name = "blog/list.html"

    def get_queryset(self):
        date = None
        if "date" in self.request.GET and self.request.GET["date"]:
            try:
                date = datetime.strptime(self.request.GET["date"], "%d-%m-%Y")
            except:
                pass

        if "tag" in self.request.GET and self.request.GET["tag"]:
            tag = self.request.GET["tag"]
            qs = TaggedItem.objects.get_union_by_model(BlogEntryTranslation,
                                                       tags=[tag])
        else:
            qs = BlogEntryTranslation.objects.all()

        if date:
            qs = qs.filter(base__published_date=date)

        qs = qs.filter(language__code=translator.current_lang)

        return qs.filter(
            base__published=True,
            base__published_date__lte=datetime.now().date(),
        )


class BlogEntryView(DetailView):

    template_name = "blog/entry.html"
    lang = translator.current_lang
    slug_field = "base__slug"
    context_object_name = "entry"

    def get_queryset(self):
        return BlogEntryTranslation.objects.filter(
            base__published=True,
            base__published_date__lte=datetime.now().date(),
            language__code=translator.current_lang
        )


class BlogCalendarView(View):

    def get(self, request, *args, **kwargs):
        try:
            month = int(request.GET.get("month", 1))
        except (ValueError, TypeError):
            month = 1

        try:
            year = int(request.GET.get("year", 2015))
        except (ValueError, TypeError):
            year = 2015

        current = datetime(year, month, 1).date()
        data = {
            "calendar": blog_calendar(current)
        }

        return JsonResponse(data)
