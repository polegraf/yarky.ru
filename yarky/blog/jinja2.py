# coding: utf-8
import calendar
from datetime import datetime, date
from django.db.models import Count
from tagging.models import Tag
from blog.models import BlogEntry, BlogEntryTranslation
from chouwa.decorators import jinja2_global, jinja2_render, jinja2_filter
from translation import translator, lazy_translate as _

CALENDAR_YEAR = [
    _('Январь'),
    _('Февраль'),
    _('Март'),
    _('Апрель'),
    _('Май'),
    _('Июнь'),
    _('Июль'),
    _('Август'),
    _('Сентябрь'),
    _('Октябрь'),
    _('Ноябрь'),
    _('Декабрь'),
]

@jinja2_global
@jinja2_render(template="blog/include/tags_cloud.html")
def blog_tags_cloud():
    cloud = Tag.objects.cloud_for_model(BlogEntryTranslation)
    context = {
        "cloud": cloud,
    }
    return context


@jinja2_global
@jinja2_render(template="blog/include/calendar.html")
def blog_calendar(current=None):

    if type(current) == str:
        try:
            current = datetime.strptime(current, "%d-%m-%Y")
        except ValueError:
            current = None
    if not current:
        current = date.today()


    previous_month = add_months(current, -1)
    next_month = add_months(current, 1)

    dates = [d.date() for d in BlogEntry.objects.values_list("published_date", flat=True)]
    blog_calendar = []

    weeks = calendar.monthcalendar(current.year, current.month)
    for week in weeks:
        blog_days = []
        for day in week:
            entries = False
            if day and date(current.year, current.month, day) in dates:
                entries = True
            blog_days.append([day, entries])
        blog_calendar.append(blog_days)

    context = {
        "previous": previous_month,
        "next": next_month,
        "current": current,
        "current_month": blog_calendar,
    }
    return context


@jinja2_global
@jinja2_render(template="blog/include/authors.html")
def blog_authors_list():
    from profile.models import PersonTranslation
    authors = PersonTranslation.objects.annotate(
        cnt=Count("base__blog_entries"),
    )
    authors = authors.filter(cnt__gt=0, language__code=translator.current_lang)
    context = {
        "authors": authors,
    }
    return context

@jinja2_global
@jinja2_render(template="blog/include/carousel.html")
def blog_carousel(count=10):
    return {
        "entries": BlogEntryTranslation.objects.filter(
            base__published=True,
            base__published_date__lte=datetime.now().date(),
            language__code=translator.current_lang,
        )[:count]
    }

@jinja2_filter
def zfill(value, len):
    return str(value).zfill(len)


def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12)
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return date(year, month, day)
