# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from tagging.fields import TagField
from translation.models import TranslationModel, TranslationGetterMixin
from translation import lazy_translate as _, translator
from yarky.helpers import HTMLField


class BlogEntry(models.Model, TranslationGetterMixin):

    title = models.CharField(max_length=255,
                             verbose_name=_(u"Системное название"),
                             default=u"Запись блога",
                             editable=False)
    author = models.ForeignKey("profile.Person", related_name="blog_entries",
                                verbose_name=_(u"Автор"))
    slug = models.SlugField(max_length=255, verbose_name=_(u"Код для URL"))
    cover = models.ImageField(upload_to="blog/covers", blank=True, null=True)

    create_date = models.DateTimeField(auto_now_add=True)
    published_date = models.DateField()

    pinned = models.BooleanField(default=False, verbose_name=_(u"Прилеплена"))
    published = models.BooleanField(default=False,
                                    verbose_name=_(u"Опубликована"))
    facebook_link = models.URLField(blank=True, null=True, editable=True)
    twitter_link = models.URLField(blank=True, null=True, editable=True)

    class Meta:
        verbose_name = u"Запись блога"
        verbose_name_plural = u"Блог"
        ordering = ["-published_date"]

    def get_absolute_url(self):
        return reverse("blog:entry",
                       kwargs={"lang": translator.current_lang, "slug": self.slug})


class BlogEntryTranslation(TranslationModel, models.Model):

    base_model = BlogEntry

    title = models.CharField(max_length=255, verbose_name=_(u"Заголовок"))
    text = HTMLField(verbose_name=_(u"Текст"))
    announce = models.TextField(verbose_name=_(u"Лид"), blank=True, default="")

    tags = TagField()

    class Meta:
        verbose_name = u"Запись блога"
        verbose_name_plural = u"Блог"
        ordering = ["-base__published_date"]

    def __str__(self):
        return self.title

    def get_tags(self):
        return map(str.strip, self.tags.split(",")) if self.tags else []

    def get_absolute_url(self):
        return self.base.get_absolute_url()

