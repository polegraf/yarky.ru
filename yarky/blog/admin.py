# coding: utf-8
from django.contrib import admin
from blog.models import BlogEntry, BlogEntryTranslation


class BlogEntryTranslationInline(admin.StackedInline):
    model = BlogEntryTranslation
    suit_classes = 'suit-tab suit-tab-translations'


class BlogEntryAdmin(admin.ModelAdmin):

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['title', 'slug', 'cover', 'published_date', 'pinned', 'published', ]
        }),
        ('Social Links', {
            'classes': ('suit-tab', 'suit-tab-links',),
            'fields': ['facebook_link', 'twitter_link']}),
        ('Translations', {
            'classes': ('suit-tab', 'suit-tab-translations',),
            'fields': ['translations']}),
    ]

    inlines = (BlogEntryTranslationInline,)
    suit_form_tabs = (('general', 'General'), ('links', 'Social Links'), ('translations', 'Translations'),)


admin.site.register(BlogEntry, BlogEntryAdmin)
