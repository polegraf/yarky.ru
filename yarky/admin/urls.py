"""
All admin urls goes through additional router
"""
from django.conf.urls import *

from .views import *
from admin.decorators import admin_required


urlpatterns = patterns('admin.views',
    url(r'^$', admin_required(AdminIndexView.as_view()), name="index"),
    # url(r'^tinymce/$', include('tinymce.urls')),
    # url(r'^tinymce/filebrowser/', include(site.urls)),
    url(r'^settings/$', admin_required(AdminSettingsView.as_view()),
        name="admin-settings"),
    url(r'^wysiwyg/upload/$', "upload_simple_image", name="admin-upload_simple_image"),
    url(r'^wysiwyg/recent/$', "recent_photos", name="admin-recent_photos"),
    url(r'^crop/$', "crop_image", name="admin-image-crop"),
    url(r'^(?P<model>[0-9a-z_\-]+)/$', "admin_router", name="default-route",
        kwargs={"action": None, "pk": None}),
    url(r'^(?P<model>[0-9a-z_\-]+)/(?P<action>[0-9a-z_\-]+)/$',
        "admin_router", name="route-action", kwargs={"pk": None}),
    url(r'^(?P<model>[0-9a-z_\-]+)/(?P<action>[0-9a-z_\-]+)/(?P<pk>[0-9]+)/$',
        "admin_router", name="route-action-object"),
)
