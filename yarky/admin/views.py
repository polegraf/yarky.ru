# -*- coding: utf-8 -*-
import logging
import os
import json

from collections import OrderedDict
from math import ceil
from random import randint
from datetime import datetime
from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.core.files.images import ImageFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db.models import FieldDoesNotExist
from django.http import HttpResponse, Http404, HttpResponseRedirect, \
    HttpResponseForbidden
from django.utils.six import StringIO
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from django.core.urlresolvers import reverse
from django.forms.models import modelform_factory, inlineformset_factory, \
    modelformset_factory
from django.contrib.auth.models import User, Group
from sorl.thumbnail import get_thumbnail

from admin.dao import get_user_type

from admin import dao
from admin.forms import (translation_form_factory, GroupForm, UserForm,
                         RelatedModelToSelectFieldForm, override_fields_in_form,
                         ColorSelectionWidget)
from blog.models import BlogEntry, BlogEntryTranslation
from content.models import Slide, SlideTranslation, SlideGroup, Page, \
    PageTranslation, TerritoryInfo, TerritoryInfoTranslation, Room, \
    RoomTranslation, SimpleImage
from galleries.models import GalleryImage, Gallery
from galleries.models import GalleryImageTranslation
from profile.models import Person, PersonTranslation
from seo.models import SeoCore

from translation.jinja2 import get_languages
from translation.models import Translation, Language

from admin.decorators import admin_required, admin_field
from variables import Setting
from yarky.utils import PossibleJSONResponseMixin, json_response

logger = logging.getLogger("debug")

class AdminFormWidget(object):
    def __init__(self, keys):
        self.title = keys.get("title", "")
        self.fields = keys.get("fields", {})
        self.translations = keys.get("translations", [])
        self.formsets = keys.get("formsets", [])
        self.size = keys.get("size", 6)
        self.collapsed = keys.get("collapsed", False)
        self.id = randint(0, 100000)

    def iter_fields(self, form):
        for field in form.visible_fields():
            if field.name in self.fields:
                yield field

    def iter_translation(self, translations):

        if self.translations:
            langs = get_languages().values_list("code", "title")
            for field in translations[langs[0][0]].visible_fields():
                if field.name in self.translations:
                    yield [(lang, form[field.name]) for lang, form in
                           translations.items()]

    def iter_formsets(self, formsets):

        for name in self.formsets:
            yield name, formsets[name]


class AdminIndexView(TemplateView):
    """
    Index view for admin interface. Here will be some cool widgets and other
    usefull staff for admins
    """
    template_name = "admin.html"

    def get_context_data(self, **kwargs):
        context = super(AdminIndexView, self).get_context_data(**kwargs)
        return context


class AdminSettingsView(TemplateView):
    template_name = "admin/settings.html"
    http_method_names = ["get", "post"]  # Allowed HTTP methods

    def post(self, *args, **kwargs):
        return self.get(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        if self.request.method == "POST":
            formset = self.get_formset()(self.request.POST)
            if formset.is_valid():
                formset.save()
                return HttpResponseRedirect("/admin/")

        else:
            formset = self.get_formset()()

        context = super(AdminSettingsView, self).get_context_data(**kwargs)
        context["formset"] = formset
        return context

    @staticmethod
    def get_formset():
        return modelformset_factory(Setting, extra=0, fields=forms.ALL_FIELDS)


@admin_required
def crop_image(request):
    from django.db.models.loading import get_model

    data = request.GET
    model = data.get("model")
    pk = data.get("pk")
    field = data.get("field")
    x = int(data.get("x"))
    y = int(data.get("y"))
    x2 = int(data.get("x2"))
    y2 = int(data.get("y2"))

    klass = get_model("meloman", model)
    try:
        obj = klass.objects.get(pk=pk)
        img = getattr(obj, field)

        path = img.path
        tmppath = u"{0}.tmp".format(path)
        os.rename(path, tmppath)

        try:
            from PIL import Image

            im = Image.open(tmppath)
            crop = im.crop((x, y, x2, y2))

            ext = img.name.split(".")[-1]

            crop_io = StringIO()
            crop.save(crop_io, format='JPEG', quality=100)

            if ext == "jpg":
                ext = "jpeg"

            ff = InMemoryUploadedFile(crop_io, None, img.name, 'image/%s' % ext,
                                      len(crop_io.getvalue()), None)
            img.delete_thumbs()
            img.save(img.name, ImageFile(ff, name=img.name), save=True)

            try:
                os.unlink(tmppath)
            except IOError:
                pass

            result = {"result": "success", "urls": [img.url, img.url_124x95]}

        except Exception:
            os.rename(tmppath, path)
            img.generate_thumbs()
            result = {"result": "fail"}

    except klass.DoesNotExist:
        result = {"result": "fail"}

    return HttpResponse(json.dumps(result), content_type="application/json")


@admin_required
@csrf_exempt
def upload_simple_image(request):
    if request.FILES.get("file"):
        image = SimpleImage.objects.create(image=request.FILES.get("file"))

        result = {"filelink": image.image.url}
    else:
        result = {}
    return HttpResponse(json.dumps(result), content_type="application/json")


@admin_required
def recent_photos(request):
    images = [
        {"thumb": obj.image.url, "image": obj.image.url}
        for obj in SimpleImage.objects.all().order_by("-id")[:50]
    ]
    return HttpResponse(json.dumps(images), content_type="application/json")


@admin_required
def admin_router(request, model, action, **kwargs):
    """
    Base view to route all admin request. Validates selected type and then
    pass request to related admin view
    """
    if dao.is_valid_type(model):
        view = dao.get_model_for_type(model,
                                      user_type=get_user_type(request.user))
        if not action:
            action = view.default_action
        return view.as_view()(request, model, action, **kwargs)
    else:
        raise Http404


# @admin_required
# def tags_list(request):
#     from tagging.models import Tag
#     tag_list = Tag.objects.filter(
#         name__icontains=request.GET.get('q')).values_list('name', flat=True)
#     return HttpResponse(json.dumps(list(tag_list)),
#                         mimetype='application/javascript')


class ModelAdminView(PossibleJSONResponseMixin, TemplateView):
    """
    Base admin view class.
    """

    model = None  # Required
    translation = False  # Is this model should be translated?
    # If "translation == True", we need translation model
    translation_model = None
    # Custom form class for adding object
    add_form = None
    # Custom form class for editing object
    edit_form = None
    # Custom formset class for related models while adding object
    add_formsets = None
    # Custom formset class for related models while editing object
    edit_formsets = None
    # Related models list. List of dicts {"model": ModelClass}
    related_models = []
    # Related forms dict.{model.__name: FormClass}
    related_forms = {}
    # extra in inlineformset_factory
    extra = 3
    # Items per page in list view
    paginate_by = 25
    form_widgets = []

    # Maybe we want to override some field's widget
    # Usage:
    # override_fields = {
    #   "fieldname": forms.widgets.Textarea
    # }
    override_fields = {}
    override_translation_fields = {}

    # Set to true to make list sortable
    sortable = False
    # field name for sorting. Required if sortable = True
    sort_field = None

    http_method_names = ["get", "post"]  # Allowed HTTP methods

    # Allowed actions.
    # TODO: add peruser permissions support, based on django.contrib
    actions = ["view", "add", "edit", "delete", "sort", "publish"]
    default_action = "view"
    published_field = None

    list_fields = []
    list_ordering = []

    templates_dict = {
        "regular": {
            # Template for objects list
            "list_template": "admin/base/list.html",
            # Template for detailed object view
            "detail_template": "admin/base/detail.html",
            # Template for object add view
            "add_template": "admin/base/add.html",
            # Template for object edit view
            "edit_template": "admin/base/edit.html",
            # Template for object edit list view
            "edit_list_template": "admin/base/edit_list.html",

        },
        "ajax": {
            # Template for objects list
            "list_template": "admin/base/include/list.html",
            # Template for detailed object view
            "detail_template": "admin/base/include/detail.html",
            # Template for object add view
            "add_template": "admin/base/include/add.html",
            # Template for object edit view
            "edit_template": "admin/base/include/edit.html",
            # Template for object edit list view
            "edit_list_template": "admin/base/include/edit_list.html",

        }
    }
    custom_templates = {}
    search_fields = []

    templates = templates_dict["regular"]

    def dispatch(self, request, _type, action, *args, **kwargs):
        """
        Checks if requested action is allowed for this model
        """

        if action not in self.actions:
            raise Http404
        self.action = action
        self.type = _type
        return super(ModelAdminView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if self.request.is_ajax():
            self.templates = self.templates_dict["ajax"]

        context = self.get_context_data()
        if isinstance(context, HttpResponseRedirect):
            return context

        if self.request.is_ajax():
            if hasattr(self, "template_name") and self.template_name:

                context = self.fill_context(context)
                response_dict = {
                    "content": self.render_to_response(context).content,
                }
            else:
                response_dict = context
            return self.render_to_json_response(response_dict)

        context = self.fill_context(context)
        return self.render_to_response(context)

    def fill_context(self, context):
        context.update({
            "type": self.type,
            "action": self.action,
            "model": self.model,
            "published_field": self.published_field,
            "sortable": self.sortable,
            "form_widgets": self.form_widgets,
            "list_fields": self.get_list_fields(),
            "search_fields": self.get_search_fields(),
        })
        return context

    def get_query_set(self):
        """
        Return base QuerySet for objects in admin
        """
        qs = self.model.objects.all()

        for search in self.search_fields:
            field = self.get_field(search)
            value = self.request.GET.get(search, None)
            if value:
                if field.__class__.__name__ in ["DateField", "DateTimeField"]:
                    qs = qs.filter(**{search: datetime.strptime(value,
                                                                "%d.%m.%Y")})
                elif field.__class__.__name__ in ["ForeignKey", ]:
                    qs = qs.filter(**{search: value})
                elif field.__class__.__name__ in ["ManyToManyField"]:
                    value = self.request.GET.getlist(search)
                    qs = qs.filter(**{"{0}__in".format(search): value})
                else:
                    qs = qs.filter(**{"{0}__icontains".format(search): value})
        if self.list_ordering:
            qs = qs.order_by(*self.list_ordering)
        return qs

    def get_object(self):
        """
        Return requested object in detailed view or raise 404.
        If object was found - cache it to memory to prevent database requests
        on every method call over request processing
        """
        if not hasattr(self, "_object"):
            try:
                self._object = self.model.objects.get(pk=self.kwargs.get("pk"))
            except self.model.DoesNotExist:
                raise Http404
        return self._object

    def soft_get_object(self):
        """
        Return object or None - doesn't raise Http404
        """
        try:
            return self.get_object()
        except Http404:
            return None

    def get_context_data(self):
        """
        Inner router. Call method depend on request action
        """
        return getattr(self, "{0}_view".format(self.action))()

    def view_view(self):
        """
        Route to list view or detailed view if pk was in request
        """
        if not self.kwargs.get("pk"):
            return self.view_list_view()
        else:
            return self.view_detail_view()

    def view_list_view(self):
        """
        Set current template to list_template and return context with QuertSet
        """
        self.template_name = self.get_templates("list_template")

        base_qs = self.get_query_set()

        try:
            page = int(self.request.GET.get("page", 1))
        except (TypeError, ValueError):
            page = 1
        if page < 1:
            page = 1

        start = (page - 1) * self.paginate_by
        stop = page * self.paginate_by

        pages_count = int(ceil(float(base_qs.count()) / self.paginate_by))
        paginated_qs = base_qs[start:stop]

        stop_after = 5
        pagination_start = page - 5
        if pagination_start < 1:
            pagination_start = 1
            stop_after = 10

        pagination_stop = page + stop_after
        if pagination_stop > pages_count:
            pagination_stop = pages_count

        return {
            "objects": paginated_qs,
            "pages_count": pages_count,
            "current_page": page,
            "pages_iterator": range(pagination_start, pagination_stop + 1),
            "search_form": self.get_search_form(),
        }

    def view_detail_view(self):
        """
        Set current template to detail_template and return context
        with QuerySet and object
        """
        self.template_name = self.get_templates("detail_template")
        return {
            "objects": self.get_query_set(),
            "object": self.get_object()
        }

    def get_form(self):
        form = modelform_factory(
            self.model,
            fields=forms.ALL_FIELDS,
            form=RelatedModelToSelectFieldForm
        )
        if self.override_fields:
            override_fields_in_form(form, self.override_fields)

        return form

    def add_view(self):
        """
        object add view
        """
        # If there is no custom add form, make it with factory
        if not self.add_form:
            self.add_form = self.get_form()
        self.template_name = self.get_templates("add_template")

        # If request was POST, try to process received data
        if self.request.method == "POST":
            if self.process_add_or_edit_forms(self.add_form):
                # If all is ok, redirects to list view

                self.after_save()

                if self.request.GET.get("inline", False):
                    return self.inline_add_response()
                return self.redirect_after_save()
        else:  # If it was GET request - prepear forms
            self.prepare_forms(self.add_form)
        context = {
            "form": self._form,
            "media": self._forms_media,
        }
        # If model should be translated, add translation forms
        # and flag to context
        if self.translation:
            context["translate"] = True
            context["translation_forms"] = self._translation_forms

        # If there is related_models for this model, add formsets
        # and flag to context
        if self.related_models:
            context["formsets"] = True
            context["formsets"] = self._formsets

        return context

    def edit_view(self):
        """
        Same as add_view in fact, instead of working with instance
        """
        # Load object
        instance = self.get_object()
        if not self.edit_form:
            self.edit_form = self.get_form()
        self.template_name = self.get_templates("edit_template")
        self._forms_media = None

        if self.request.method == "POST":
            if self.process_add_or_edit_forms(self.edit_form):
                if self.request.GET.get("inline", False):
                    return self.inline_edit_response()
                return self.redirect_after_save()
        else:
            self.prepare_forms(self.edit_form)
        context = {
            "instance": instance,
            "form": self._form,
            "media": self._forms_media,
        }

        if self.translation:
            context["translate"] = True
            context["translation_forms"] = self._translation_forms
        if self.related_models:
            context["formsets"] = True
            context["formsets"] = self._formsets
        return context

    def redirect_after_save(self):
        back = None
        if "back" in self.request.POST:
            back = self.request.POST.get("back")

        if "continue" in self.request.POST:
            url = reverse("admin:route-action-object",
                          args=[self.type, "edit", self._object.pk])
            if back:
                url += u"?back={0}".format(back)
            return HttpResponseRedirect(url)
        elif back:
            return HttpResponseRedirect(back)

        return self.redirect_to_list()

    def prepare_forms(self, form_class, data=None, files=None):
        """
        Forms preparation method.
        Creates forms instances (add, edit)
        Creates translation models (if required) using factory
        """
        instance = self.soft_get_object()
        users_initial = {}
        if not hasattr(instance, "user"):
            users_initial["user"] = self.request.user
        else:
            users_initial["user"] = instance.user
        if not hasattr(instance, "author"):
            users_initial["author"] = self.request.user
        else:
            users_initial["author"] = instance.author

        self._form = form_class(data=data, files=files, instance=instance,
                                initial=users_initial)
        self._forms_media = self._form.media

        # Prepare Translations
        if self.translation:
            self.translation_form = translation_form_factory(
                self.translation_model, self.override_translation_fields
            )
            self._translation_forms = OrderedDict()
            # One instance for each language
            for lang in get_languages():
                translation_instance = None
                if instance:
                    try:
                        translation_instance = instance.translations.get(
                            language=lang
                        )
                    except:
                        raise
                translation_form = self.translation_form(
                    data=data,
                    files=files,
                    prefix=lang.code,
                    initial={"language": lang.pk},
                    instance=translation_instance
                )
                self._translation_forms.update({lang.code: translation_form})
                # Don't forget to update forms media (js/css)
                self._forms_media += translation_form.media

        # Prepare other formsets for related_models
        # using inlineformset_factory
        if self.related_models:
            self._formsets = None
            self._formsets = {}
            for relation in self.related_models:
                # (field name, related_name) or None
                related_field = dao.get_relatation_field_names(
                    relation["model"], self.model
                )
                model_name = relation["model"].__name__
                if not related_field:
                    raise TypeError(
                        "Model '{0}' has not relation with '{1}'".format(
                            relation["model"].__name__, self.model.__name__
                        )
                    )

                kw = {'form': self.related_forms[model_name]} \
                    if self.related_forms else {}
                formset_class = inlineformset_factory(
                    self.model, relation["model"], fields=forms.ALL_FIELDS,
                    can_delete=True, extra=self.extra, **kw)
                formset = formset_class(data=data, files=files,
                                        instance=instance, prefix=model_name)
                self._formsets.update({model_name: formset})

    def process_add_or_edit_forms(self, form_class):
        """
        Processing add/edit forms while POST request
        """
        # Prepere all forms

        self.prepare_forms(form_class, self.request.POST, self.request.FILES)

        logger.debug("="*80)
        logger.debug(self.request.path)
        logger.debug("-"*80)
        logger.debug(self.request.POST)
        logger.debug("-"*80)
        logger.debug(self.request.META)
        logger.debug("="*80)


        # By default all translations add related formsets are valid
        # to simple compatibility with models without translations or relations
        translation_forms_valid = True
        formsets_valid = True

        # If there ought to be translations - validate formsets for
        # all languages
        # If any of them is invalid - set flag to False
        if self.translation:
            for lang_code, translation_form in self._translation_forms.items():
                if not translation_form.is_valid():
                    translation_forms_valid = False

        # Same thing as with translations
        if self.related_models:
            for related_name, formset in self._formsets.items():
                if not formset.is_valid():
                    formsets_valid = False

        # So, if there is no errors in main form, translations and relations
        if self._form.is_valid() and translation_forms_valid and formsets_valid:
            # Save objects
            self._object = self._form.save(commit=False)
            self.before_save()
            self._object.save()
            self._form.save_m2m()

            # Save translations (if required)
            if self.translation:
                for lang_code, translation_form in self._translation_forms.items():
                    translation = translation_form.save(commit=False)
                    # Don't forget to add base object
                    translation.base = self._object
                    translation.save()
                    # Auto update `system title` from first language
                    # Check if there is such field and it is not editable
                    # and if `system title` was not already updated by another
                    # language
                    # pep8 will not be happy =(
                    if hasattr(self._object, "title") and not \
                            self._object._meta.get_field_by_name("title")[
                                0].editable \
                            and not getattr(self._object, "_title_updated",
                                            False):
                        self._object.title = str(translation)
                        self._object.save(update_fields=["title"])
                        # Add private attr to mark title updated
                        self._object._title_updated = True

            # Save related fields
            if self.related_models:
                for related_field_name, formset in self._formsets.items():
                    formset.instance = self._object
                    formset.save()

            self.rebuild_cache(self._object)
            return True

        return False

    def delete_view(self):
        """
        Delete. Just delete
        """
        self.get_object().delete()
        return self.redirect_to_list()

    def redirect_to_list(self):
        """
        Return redirect response
        """
        return HttpResponseRedirect(reverse("admin:route-action",
                                            args=[self.type, "view"]))

    def inline_add_response(self):
        self.template_name = "admin/inline_add_response.html"
        context = {
            "object": self._object,
        }
        return context

    def inline_edit_response(self):
        self.template_name = "admin/inline_add_response.html"
        context = {
            "object": self._object,
        }
        return context

    def sort_view(self):
        if not self.sortable:
            return Http404

        ids = self.request.POST.getlist("ids[]")
        for i in range(len(ids)):
            self.model.objects.filter(id=ids[i]).update(**{self.sort_field: i})

        return {"success": "true"}

    def publish_view(self):
        if not self.published_field:
            return Http404

        obj = self.get_object()
        current = getattr(obj, self.published_field)
        setattr(obj, self.published_field, not current)
        obj.save()

        return {"success": "true"}

    def get_templates(self, name):
        return self.custom_templates.get(name, self.templates[name])

    def get_search_fields(self):
        return [self.get_field(f) for f in self.search_fields]

    def get_list_fields(self):
        return [(self.get_field(f), islink) for f, islink in self.list_fields]

    def get_field(self, name):
        try:
            return self.model._meta.get_field_by_name(name)[0]
        except FieldDoesNotExist:
            return getattr(self, name)

    def get_search_form(self):
        class SearchForm(forms.ModelForm):

            def __init__(self, *args, **kwargs):
                super(SearchForm, self).__init__(*args, **kwargs)
                for field in self.fields.values():
                    if field.__class__.__name__ in ["DateField",
                                                    "DateTimeField"]:
                        css = field.widget.attrs.get("class", "")
                        css += " datepicker input-small"
                        field.widget.attrs["class"] = css

            class Meta:
                model = self.model
                fields = self.search_fields

        return SearchForm(self.request.GET)

    def rebuild_cache(self, obj):
        pass

    def before_save(self):
        pass

    def after_save(self):
        pass


class TranslationAdminView(ModelAdminView):
    model = Translation

    def view_list_view(self):
        """
        Set current template to list_template and return context with QuertSet
        """
        lang = self.request.GET.get('lang')
        self.template_name = self.get_templates("edit_list_template")
        query_set = self.get_query_set()
        trans = modelformset_factory(self.model, extra=0, exclude=('language',),
                                     fields=forms.ALL_FIELDS,
                                     form=RelatedModelToSelectFieldForm)
        objects = query_set.filter(language__code=lang) if lang else query_set
        if self.request.method == "POST":
            formsets = trans(self.request.POST, self.request.FILES,
                             queryset=objects)
            if formsets.is_valid():
                formsets.save()
        else:
            formsets = trans(queryset=objects)
        for form in formsets.forms:
            form.fields["key"].widget.attrs["rows"] = 1
            form.fields["value"].widget.attrs["rows"] = 1

        return {"objects": objects, 'formsets': formsets}


class LanguageAdminView(ModelAdminView):
    model = Language


class UserAdmin(ModelAdminView):
    model = User
    add_form = UserCreationForm
    edit_form = UserForm

    list_fields = [("username", 1), ("get_groups", 0), ("change_password_link", 0)]

    @admin_field(u"Группы")
    def get_groups(self, instance):
        return ", ".join(instance.groups.values_list("name", flat=True))

    @admin_field(u"", safe=True)
    def change_password_link(self, instance):
        return "<a href='{}'>Сменить пароль</a>".format(reverse("password_change", args=[instance.id]))


class SeoCoreAdminView(ModelAdminView):
    model = SeoCore
    list_fields = [("language", 1), ]


class GroupAdmin(ModelAdminView):
    model = Group
    add_form = GroupForm
    edit_form = GroupForm

    list_fields = [("name", 1)]

    actions = ["view", "add", "edit", "delete"]


class BlogEntryAdminView(ModelAdminView):
    model = BlogEntry
    translation = True
    translation_model = BlogEntryTranslation

    override_translation_fields = {
        "title": forms.Textarea
    }

    published_field = "published"
    list_fields = [("title", 1)]
    #actions = ["view", "add", "edit", "delete"]

    form_widgets = [
        AdminFormWidget({
            "title": u"Информация",
            "fields": ["title", "slug", "published", "published_date",
                       "pinned", "cover", "author"],
            "translations": ["tags", ],
        }),
        AdminFormWidget({
            "title": u"Информация",
            "translations": ["title", "announce"],
        }),
        AdminFormWidget({
            "size": 12,
            "title": u"Контент",
            "translations": ["text", ],
        })
    ]

    def before_save(self):
        if not self._object.author:
            self._object.author = self.request.user.person


class PersonAdminView(ModelAdminView):
    model = Person
    translation = True
    translation_model = PersonTranslation

    list_fields = (("get_name", 1), )

    @admin_field(u"Имя")
    def get_name(self, obj):
        return obj.get_local_get_name()


class GalleryAdminView(ModelAdminView):
    model = Gallery

    actions = ["view", "add", "edit", "delete", "sort", "update", "upload"]

    templates = {
        "list_template": "/admin/custom/gallery/list.html",
        "detail_template": "admin/base/detail.html",
        "add_template": "admin/base/add.html",
        "edit_template": "admin/base/edit.html",
        "edit_list_template": "admin/base/edit_list.html",
    }

    def update_view(self):
        return {"success": "true"}

    def upload_view(self):
        GalleryImageForm = modelform_factory(GalleryImage, fields=forms.ALL_FIELDS)
        GalleryImageTranslationForm = translation_form_factory(GalleryImageTranslation, {})

        results = {}

        for file in self.request.FILES.values():

            position = 0
            if self.get_object().images.exists():
                position = self.get_object().images.latest("position").position

            form = GalleryImageForm({
                "gallery": self.get_object().id,
                "position": position,
            }, {
                "image": file
            })
            if form.is_valid():
                image = form.save()
                for language in get_languages():
                    translation_form = GalleryImageTranslationForm({
                        "language": language.id,
                    })
                    if translation_form.is_valid():
                        trans = translation_form.save(commit=False)
                        trans.base = image
                        trans.save()
                results[file.name] = {
                    "url": get_thumbnail(image.image, "150x150", crop="center").url,
                    "id": image.id,
                    "edit": reverse("admin:route-action-object", args=['galleryimage', 'edit', image.pk]),
                    "delete": reverse("admin:route-action-object", args=['galleryimage', 'delete', image.pk])
                }

        return {"success": "true", "images": results}


class GalleryImageAdminView(ModelAdminView):
    model = GalleryImage
    list_fields = [("get_image", 1), ]
    translation = True
    translation_model = GalleryImageTranslation
    sortable = True
    sort_field = "position"

    search_fields = ["gallery"]

    @admin_field(verbose=u"Превью", safe=True)
    def get_image(self, obj):
        return "<img src='{0}' />".format(get_thumbnail(obj.image, "200").url)


class SlideGroupAdminView(ModelAdminView):
    model = SlideGroup


class SlideAdminView(ModelAdminView):
    model = Slide
    translation = True
    translation_model = SlideTranslation

    search_fields = ["group"]

    sortable = True
    sort_field = "position"
    published_field = "published"

    override_fields = {
        "background": ColorSelectionWidget,
        "title_color": ColorSelectionWidget,
        "subtitle_color": ColorSelectionWidget,
    }
    override_translation_fields = {
        "title": forms.Textarea
    }
    list_fields = [("get_image", 1), ("group", 0)]

    @admin_field(verbose=u"Превью", safe=True)
    def get_image(self, obj):
        if obj.image:
            return "{1}<br/><img src='{0}' />".format(
                get_thumbnail(obj.image, "124").url,
                obj.title
            )
        return ""


class PageAdminView(ModelAdminView):
    model = Page
    translation = True
    translation_model = PageTranslation


class TerritoryInfoAdminView(ModelAdminView):
    model = TerritoryInfo
    translation = True
    translation_model = TerritoryInfoTranslation
    sortable = True
    sort_field = "position"
    published_field = "published"

    def get_query_set(self):
        base_qs = super().get_query_set()
        if "territory" in self.request.GET:
            base_qs = base_qs.filter(territory=self.request.GET["territory"])
        return base_qs

    def view_list_view(self):
        if "territory" not in self.request.GET:
            self.template_name = "admin/custom/territory/select_territory.html"
            return {"type": self.type}
        else:
            return super().view_list_view()


class RoomAdminView(ModelAdminView):

    model = Room
    translation = True
    translation_model = RoomTranslation
    sortable = True
    sort_field = "position"
    published_field = "published"



# Associate AdminViews with models
dao.register(Language, LanguageAdminView)
dao.register(Translation, TranslationAdminView)
dao.register(User, UserAdmin)
dao.register(Group, GroupAdmin)
dao.register(SeoCore, SeoCoreAdminView)
dao.register(BlogEntry, BlogEntryAdminView)
dao.register(Person, PersonAdminView)
dao.register(Gallery, GalleryAdminView)
dao.register(GalleryImage, GalleryImageAdminView)
dao.register(Slide, SlideAdminView)
dao.register(SlideGroup, SlideGroupAdminView)
dao.register(Page, PageAdminView)
dao.register(TerritoryInfo, TerritoryInfoAdminView)
dao.register(Room, RoomAdminView)
