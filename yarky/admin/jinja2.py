# coding: utf-8
import urllib
from chouwa.decorators import jinja2_global, jinja2_filter

def _process_field_attributes(field, attr, process):

    # split attribute name and value from 'attr:value' string
    params = attr.split(':', 1)
    attribute = params[0]
    value = params[1] if len(params) == 2 else ''

    # decorate field.as_widget method with updated attributes
    old_as_widget = field.as_widget

    def as_widget(self, widget=None, attrs=None, only_initial=False):
        attrs = attrs or {}
        process(widget or self.field.widget, attrs, attribute, value)
        return old_as_widget(widget, attrs, only_initial)

    bound_method = type(old_as_widget)
    field.as_widget = bound_method(as_widget, field, field.__class__)
    return field


@jinja2_filter
def set_attr(field, attr):
    def process(widget, attrs, attribute, value):
        attrs[attribute] = value
    return _process_field_attributes(field, attr, process)


@jinja2_filter
def append_attr(field, attr):
    def process(widget, attrs, attribute, value):
        if attrs.get(attribute):
            attrs[attribute] += ' ' + value
        elif widget.attrs.get(attribute):
            attrs[attribute] = widget.attrs[attribute] + ' ' + value
        else:
            attrs[attribute] = value
    return _process_field_attributes(field, attr, process)


@jinja2_global
def is_valid_type(_type, user):
    from admin.dao import get_user_type, is_valid_type as _is_valid_type
    return _is_valid_type(_type, get_user_type(user))


@jinja2_filter
def add_class(field, css_class):
    return append_attr(field, 'class:'+ css_class)


@jinja2_filter
def add_error_class(field, css_class):
    if hasattr(field, 'errors') and field.errors:
        return add_class(field, css_class)
    return field


@jinja2_filter
def set_data(field, data):
    return set_attr(field, 'data-' + data)


@jinja2_filter
def behave(field, names):
    """ https://github.com/anutron/behavior support """
    return set_data(field, 'filters:'+names)


@jinja2_global
def field_type(field):
    return field.field.widget.__class__.__name__


@jinja2_global
def build_pagination_querystring(request, page):
    qs = [u"{0}={1}".format(key, value) for key, value in request.GET.items()
          if not key == "page"]
    qs.append("page={0}".format(page))
    return u"&".join(qs)


@jinja2_filter
def quote(path):
    return urllib.parse.quote_plus(path)


@jinja2_filter
def get_field_val(obj, field):
    if callable(field):
        return field(obj)
    val = getattr(obj, field.name, obj)
    return val

@jinja2_global
def get_attribute(obj, attr, default):
    return getattr(obj, attr, default)
