# coding: utf-8
from random import randint

from django import forms
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.contrib.auth.models import Group, User
from sorl.thumbnail import get_thumbnail

from translation.models import Language


class RelatedModelToSelectFieldForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RelatedModelToSelectFieldForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():

            if isinstance(field, forms.ImageField):
                field.widget = AdminImageWidget(attrs=field.widget.attrs)

            if isinstance(field, (forms.ModelChoiceField,
                                  forms.ModelMultipleChoiceField)):
                field.widget.attrs.update(
                    {"data-model": field.queryset.model.__name__.lower()}
                )

            if isinstance(field, forms.ModelChoiceField) and not \
                    isinstance(field, forms.ModelMultipleChoiceField):
                field.widget.attrs.update({"class": 'select2'})

            if isinstance(field, (forms.DateField, forms.DateTimeField)):
                field.widget.attrs.update({"class": "datepicker input-small"})

    def full_clean(self, *args, **kwargs):
        # self.data = dict(self.data.items())
        for name, field in self.fields.items():
            if field.__class__.__name__ == "CharField":
                name = self.add_prefix(name)
                value = self.data.get(name)
                if value:
                    try:
                        self.data[name] = value.strip()
                    except AttributeError:
                        self.data = self.data.copy()
                        self.data[name] = value.strip()

        super(RelatedModelToSelectFieldForm, self).full_clean()


def translation_form_factory(translation_model, override=None):
    """
    Factory to create tranlations forms from models
    """

    class TranslationEditForm(RelatedModelToSelectFieldForm):
        language = forms.ModelChoiceField(queryset=Language.objects.all(),
                                          widget=forms.HiddenInput)

        class Meta:
            model = translation_model
            # Exclude SEO fields for now
            exclude = ('page_title', 'page_description', 'page_keywords')

    return override_fields_in_form(TranslationEditForm, override)


def override_fields_in_form(form, override_fields):
    for name, field in form.base_fields.items():
        if name in override_fields:
            kwargs = {"attrs": field.widget.attrs}
            if isinstance(field.widget, forms.Select):
                kwargs.update({"choices": field.widget.choices})
            field.widget = override_fields[name](**kwargs)
    return form


class GroupForm(RelatedModelToSelectFieldForm):

    class Meta:
        model = Group
        exclude = ("permissions",)


from django.contrib.admin.widgets import AdminFileWidget


class AdminImageWidget(forms.ClearableFileInput):

    template_with_initial = (
        '%(clear_template)s <br />  %(input)s'
    )

    template_with_clear = '%(clear)s <label for="%(clear_checkbox_id)s" style="display: inline;">%(clear_checkbox_label)s</label>'

    def render(self, name, value, attrs=None):
        output = super(AdminImageWidget, self).render(name, value, attrs)
        if value and getattr(value, "url", None):
            url, url_124x95 = value.url, get_thumbnail(value, "124", crop="center").url
            link = u'<table><tr><td>' \
                   u'<a href="{href}?_={randint}" target="_blank" ' \
                   u'data-field="{name}" ' \
                   u'data-model="{model}" data-id="{id}">' \
                   u'<img src="{src}" /></a>' \
                   u'</td><td>{form}</td></tr></table>'
            output = link.format(
                href=url, src=url_124x95, name=value.field.name,
                model=value.instance.__class__.__name__,
                id=value.instance.id, randint=randint(0, 1000000),
                form=output
            )


        return mark_safe(output)


class UserForm(RelatedModelToSelectFieldForm):

    class Meta:
        model = User
        exclude = ('user_permissions', )


class AutoSlugInput(forms.TextInput):

    def __init__(self, *args, **kwargs):
        source = kwargs.pop("source", "title")
        super(AutoSlugInput, self).__init__(*args, **kwargs)
        self.attrs.update({"data-title": source, "class": "autoslug"})


class ColorSelectionWidget(forms.Select):

    def __init__(self, attrs, choices=()):
        attrs.setdefault("class", "")
        attrs["class"] += " nonselect2 select-color"
        super().__init__(attrs, choices)
