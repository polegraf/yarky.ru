# coding: utf-8
from django.db import models

# Global singleton
admin_models = {}
tickets_admin_models = {}
webcasts_admin_models = {}


def get_admin_models():
    """
    Inspects installed models for admin
    *Not used
    """
    result = {}
    for model in models.get_models():
        if getattr(model, "admin", False):
            result.update({model.__name__.lower(): model.admin_view})
    return result


def is_valid_type(_type, user_type="common"):
    """
    Check if is registed as admin
    """
    return _type in admin_models


def get_model_for_type(_type, user_type="common"):
    """
    Return admin view class by type name
    """
    if is_valid_type(_type, user_type):
        if user_type == "tickets":
            return tickets_admin_models[_type]
        elif user_type == "webcasts":
            return webcasts_admin_models[_type]
        return admin_models[_type]


def get_model_for_class(cls):
    """
    Return admin view class by model
    """
    return get_model_for_type(cls.__name__.lower())


def get_model_for_object(obj):
    """
    Return admin view class by instance
    """
    return get_model_for_class(obj.__class__)


def register(model, admin_model, _type="common"):
    """
    Add model/admin_view key pair to singleton
    """
    if _type == "tickets":
        tickets_admin_models.update({model.__name__.lower(): admin_model})
    elif _type == "webcasts":
        webcasts_admin_models.update({model.__name__.lower(): admin_model})
    else:
        admin_models.update({model.__name__.lower(): admin_model})


def get_relatation_field_names(model, related_to):
    """
    Try to find field names for OneToOne, OneToMany, ManyToMany relations
    between two models
    """
    for field in model._meta.fields:
        if field.rel is not None and field.rel.to == related_to:
            return field.name, field.rel.related_name
    return None


def get_user_type(user):
    if not hasattr(user, "_user_type"):
        user._user_type = "common"
    return user._user_type

