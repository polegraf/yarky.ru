# coding: utf-8
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from django.core.urlresolvers import reverse
from django.core.validators import BaseValidator
from django.db import models
from translation.models import TranslationGetterMixin, TranslationModel
from yarky.helpers import HTMLField


class ImageHeightValidator(BaseValidator):

    def __init__(self, height):
        super().__init__(height)
        self.height = height

    def __call__(self, image):
        if not image:
            return

        _, height = get_image_dimensions(image)

        if not self.height == height:
            raise ValidationError(
                u"Высота картинки должна быть {0}".format(self.height)
            )


class SlideGroup(models.Model):

    key = models.SlugField(max_length=32, verbose_name=u"Ключ группы")

    class Meta:
        verbose_name = u"Группа слайдов"
        verbose_name_plural = u"Группы слайдов"

    def __str__(self):
        return self.key


class Slide(TranslationGetterMixin, models.Model):

    title = models.CharField(max_length=150,
                             verbose_name=u"Системное название",
                             editable=False, default="Слайд")
    group = models.ForeignKey(SlideGroup, related_name="slides")
    image = models.ImageField(upload_to="slides/",
                              verbose_name=u"Картинка",
                              validators=[ImageHeightValidator(height=565)])
    colors = (
        ("darkorange", "Оранжевый"),
        ("brightgreen", "Зеленый"),
        ("acidpink", "Кислотно-розовый"),
        ("pink", "Розовый"),
        ("aqua", "Морской"),
        ("cyan", "Голубой"),
        ("gray", "Серый"),
        ("black", "Черный"),
        ("white", "Белый"),
    )
    background = models.CharField(max_length=32, choices=colors,
                                  verbose_name=u"Цвет фона")
    title_color = models.CharField(max_length=32, choices=colors,
                                   verbose_name=u"Цвет заголовка",
                                   default="black")
    subtitle_color = models.CharField(max_length=32, choices=colors,
                                      verbose_name=u"Цвет текста",
                                      default="white")

    published = models.BooleanField(default=True, verbose_name=u"Опубликовано")
    position = models.PositiveSmallIntegerField(default=0,
                                                verbose_name=u"Позиция",
                                                editable=False)

    class Meta:
        verbose_name = u"Слайд"
        verbose_name_plural = u"Слайды"
        ordering = ["position"]

    def __str__(self):
        return self.title


class SlideTranslation(TranslationModel):

    base_model = Slide

    title = models.CharField(max_length=128, verbose_name=u"Заголовок",
                             blank=True, default="")
    subtitle = HTMLField(verbose_name=u"Подзаголовок", blank=True,
                         default="")

    class Meta:
        verbose_name = u"Слайд"
        verbose_name_plural = u"Слайды"
        ordering = ["base__position"]

    def __str__(self):
        return self.title


class Page(models.Model, TranslationGetterMixin):

    title = models.CharField(max_length=150, verbose_name=u"Системное название",
                             unique=True, editable=False, default=u"Статья")
    slug = models.SlugField(max_length=150, verbose_name=u"Код для URL",
                            unique=True, db_index=True)

    inline = models.BooleanField(default=False, verbose_name="Скрытая")
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = u"Статья"
        verbose_name_plural = u"Статьи"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("article", args=[self.slug])


class PageTranslation(TranslationModel):

    base_model = Page

    title = models.CharField(max_length=150, verbose_name=u"Заголовок")
    content = HTMLField(verbose_name=u"Содержание", blank=True, default="")

    class Meta:
        verbose_name = u"Статья"
        verbose_name_plural = u"Статьи"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return self.base.get_absolute_url()


class Reservation(models.Model):
    date_in = models.DateField()
    date_out = models.DateField()
    guests = models.PositiveSmallIntegerField()

    breakfasts = (
        ('0', 'Не включены'),
        ('1', 'Включены')
    )
    breakfast = models.CharField(max_length=1, choices=breakfasts)
    room_type = models.ForeignKey("Room")
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    phone = models.CharField(max_length=64)
    email = models.EmailField()
    comment = models.TextField(blank=True, null=True)

    def get_price(self):
        return self.room_type.price

    def total_cost(self):
        return (self.get_price()+int(self.breakfast)*180)*self.guests


class TerritoryInfo(TranslationGetterMixin, models.Model):

    title = models.CharField(max_length=128,
                             verbose_name=u"Системное название",
                             editable=False)
    territories = (
        ("green", "Зеленая"),
        ("red", "Красная"),
        ("blue", "Синяя"),
        ("yellow", "Желтая"),
    )
    territory = models.CharField(max_length=10, verbose_name=u"Территория",
                                 choices=territories)
    collapsable = models.BooleanField(default=True,
                                      verbose_name=u"Сворачивается")
    position = models.PositiveSmallIntegerField(default=0,
                                                verbose_name=u"Позиция",
                                                editable=False)
    published = models.BooleanField(verbose_name=u"Опубликовано", default=True)

    class Meta:
        verbose_name = u"Территория"
        verbose_name_plural = u"Территории"
        ordering = ["territory", "position"]

    def __str__(self):
        return self.title


class TerritoryInfoTranslation(TranslationModel):

    base_model = TerritoryInfo

    title = models.CharField(max_length=128, verbose_name=u"Заголовок")
    text = HTMLField(verbose_name=u"Описание")

    class Meta:
        verbose_name = u"Территория"
        verbose_name_plural = u"Территории"
        ordering = ["base__territory", "base__position"]

    def __str__(self):
        return self.title



class Room(models.Model, TranslationGetterMixin):

    title = models.CharField(max_length=150, verbose_name=u"Системное название",
                             editable=False, default="Номер")

    places = models.PositiveSmallIntegerField(verbose_name=u"Количество мест")
    price = models.PositiveIntegerField(verbose_name=u"Цена за ночь")

    image = models.ImageField(upload_to="rooms/", verbose_name=u"Фото")

    published = models.BooleanField(default=True, verbose_name=u"Опубликовано")
    position = models.PositiveSmallIntegerField(default=0,
                                                verbose_name=u"Позиция",
                                                editable=False)

    class Meta:
        verbose_name = u"Номер"
        verbose_name_plural = u"Номера"
        ordering = ["position"]

    def __str__(self):
        return self.title


class RoomTranslation(TranslationModel):

    base_model = Room

    title = models.CharField(max_length=150, verbose_name=u"Заголовок")
    short_title = models.CharField(max_length=150,
                                   verbose_name=u"Короткий заголовок")
    description = HTMLField(verbose_name=u"Описание")

    class Meta:
        verbose_name = u"Номер"
        verbose_name_plural = u"Номера"
        ordering = ["base__position"]

    def __str__(self):
        return self.title


class SimpleImage(models.Model):
    image = models.ImageField(upload_to="images/")
