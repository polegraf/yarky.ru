from django.template.defaultfilters import striptags
from sorl.thumbnail import get_thumbnail
from chouwa.decorators import jinja2_filter, jinja2_global, jinja2_render
from content.models import SlideTranslation, PageTranslation, \
    TerritoryInfoTranslation, RoomTranslation
from translation import translator


@jinja2_filter
def allow_br(title):
    title = title.replace("<br>", "__br__").replace("&nbsp;", "__nbsp__")
    title = striptags(title)
    return title.replace("__br__", "<br>").replace("__nbsp__", "&nbsp;")


@jinja2_filter
def clear_br(title):
    return title.replace("<br>", " ").replace("&nbsp;", " ")

@jinja2_filter
def clear_linebreaks(title):
    return title.replace("\r", "").replace("\n", " ")


@jinja2_global
@jinja2_render(template="slider.html")
def slider(key, html_id):
    return {
        "id": html_id,
        "slides": SlideTranslation.objects.filter(
            base__group__key=key,
            language__code=translator.current_lang,
            base__published=True
        ),
    }

@jinja2_global
def page_inline(url):
    try:
        return PageTranslation.objects.get(
            language__code=translator.current_lang, base__slug=url).content
    except PageTranslation.DoesNotExist:
        return None

@jinja2_global
def page_inline_obj(url):
    try:
        return PageTranslation.objects.get(
            language__code=translator.current_lang, base__slug=url)
    except PageTranslation.DoesNotExist:
        return ''


@jinja2_global
def thumbnail(*args, **kwargs):
    return get_thumbnail(*args, **kwargs)


@jinja2_global
@jinja2_render(template="include/territory.html")
def territory_info():
    return {
        "territory": TerritoryInfoTranslation.objects.filter(
            language__code=translator.current_lang,
            base__published=True
        )
    }

@jinja2_global
@jinja2_render(template="include/rooms_widget.html")
def rooms_widget():
    return {
        "rooms": RoomTranslation.objects.filter(
            language__code=translator.current_lang,
            base__published=True
        )
    }

@jinja2_global
@jinja2_render(template="include/rooms_select.html")
def rooms_select():
    return {
        "rooms": RoomTranslation.objects.filter(
            language__code=translator.current_lang,
            base__published=True
        )
    }

