import json
from django.core.mail import send_mail
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from content.forms import ReservationForm
from translation import lazy_translate as _
from yarky import settings


class HomeView(TemplateView):
    template_name = "home.html"


# Temporary view for booking
@csrf_exempt
def reservation(request):
    if request.method == "POST":
        form = ReservationForm(request.POST)
        if form.is_valid():
            reservation = form.save()

            context = {
                "reservation": reservation,
            }
            content = render_to_string(
                "include/reservation_result.html", context, request=request
            )
            customer_content = render_to_string(
                "mails/reservation_customer.txt", context, request=request
            )
            manager_content = render_to_string(
                "mails/reservation_customer.txt", context, request=request
            )

            send_mail(u"Новая заявка на бронирование (от: Yarkyhostel.com)",
                      manager_content, None, settings.RESERVATION_EMAILS)
            send_mail(_(u"от Yarky Hostel & Space -  Ваша заявка на бронирование"), customer_content,
                      None, [reservation.email])


            data = {
                "content": content,
                "success": "success"
            }
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            errors = form.errors
            return HttpResponse(json.dumps(errors), content_type="application/json")

