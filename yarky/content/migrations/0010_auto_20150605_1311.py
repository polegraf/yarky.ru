# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0009_auto_20150604_2137'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='slide',
            options={'ordering': ['-position'], 'verbose_name_plural': 'Слайды', 'verbose_name': 'Слайд'},
        ),
        migrations.AlterModelOptions(
            name='slidetranslation',
            options={'ordering': ['-base__position'], 'verbose_name_plural': 'Слайды', 'verbose_name': 'Слайд'},
        ),
        migrations.AddField(
            model_name='slide',
            name='position',
            field=models.PositiveSmallIntegerField(verbose_name='Позиция', default=0),
        ),
        migrations.AddField(
            model_name='slide',
            name='published',
            field=models.BooleanField(verbose_name='Опубликовано', default=True),
        ),
        migrations.AlterField(
            model_name='page',
            name='title',
            field=models.CharField(editable=False, max_length=150, unique=True, verbose_name='Системное название', default='Статья'),
        ),
        migrations.AlterField(
            model_name='slide',
            name='title',
            field=models.CharField(editable=False, max_length=150, verbose_name='Системное название', default='Слайд'),
        ),
        migrations.AlterUniqueTogether(
            name='slidetranslation',
            unique_together=set([]),
        ),
    ]
