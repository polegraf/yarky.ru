# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0008_auto_20150604_2136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='subtitle_color',
            field=models.CharField(max_length=32, verbose_name='Цвет текста', choices=[('darkorange', 'Оранжевый'), ('brightgreen', 'Зеленый'), ('acidpink', 'Кислотно-розовый'), ('pink', 'Розовый'), ('aqua', 'Морской'), ('cyan', 'Голубой'), ('gray', 'Серый'), ('black', 'Черный'), ('white', 'Белый')], default='white'),
        ),
        migrations.AlterField(
            model_name='slide',
            name='title_color',
            field=models.CharField(max_length=32, verbose_name='Цвет заголовка', choices=[('darkorange', 'Оранжевый'), ('brightgreen', 'Зеленый'), ('acidpink', 'Кислотно-розовый'), ('pink', 'Розовый'), ('aqua', 'Морской'), ('cyan', 'Голубой'), ('gray', 'Серый'), ('black', 'Черный'), ('white', 'Белый')], default='black'),
        ),
    ]
