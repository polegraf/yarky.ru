# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0007_auto_20150528_0736'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='subtitle_color',
            field=models.CharField(default='white', verbose_name='Цвет текста', choices=[('darkorange', 'Оранжевый'), ('brightgreen', 'Зеленый'), ('acidpink', 'Кислотно-розовый'), ('pink', 'Розовый'), ('aqua', 'Морской'), ('cyan', 'Голубой'), ('gray', 'Серый'), ('black', 'Черный'), ('white', 'Белый')], max_length=32),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='slide',
            name='title_color',
            field=models.CharField(default='black', verbose_name='Цвет заголовка', choices=[('darkorange', 'Оранжевый'), ('brightgreen', 'Зеленый'), ('acidpink', 'Кислотно-розовый'), ('pink', 'Розовый'), ('aqua', 'Морской'), ('cyan', 'Голубой'), ('gray', 'Серый'), ('black', 'Черный'), ('white', 'Белый')], max_length=32),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='slide',
            name='background',
            field=models.CharField(verbose_name='Цвет фона', choices=[('darkorange', 'Оранжевый'), ('brightgreen', 'Зеленый'), ('acidpink', 'Кислотно-розовый'), ('pink', 'Розовый'), ('aqua', 'Морской'), ('cyan', 'Голубой'), ('gray', 'Серый'), ('black', 'Черный'), ('white', 'Белый')], max_length=32),
        ),
    ]
