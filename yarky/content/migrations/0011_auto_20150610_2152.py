# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import yarky.helpers


class Migration(migrations.Migration):

    dependencies = [
        ('translation', '__first__'),
        ('content', '0010_auto_20150605_1311'),
    ]

    operations = [
        migrations.CreateModel(
            name='TerritoryInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128, verbose_name='Системное название', editable=False)),
                ('territory', models.CharField(max_length=10, verbose_name='Территория', choices=[('green', 'Зеленая'), ('red', 'Красная'), ('blue', 'Синяя'), ('yellow', 'Желтая')])),
                ('collapsable', models.BooleanField(verbose_name='Сворачивается', default=True)),
                ('position', models.PositiveSmallIntegerField(verbose_name='Позиция', editable=False, default=0)),
            ],
            options={
                'verbose_name': 'Территория',
                'verbose_name_plural': 'Территории',
            },
        ),
        migrations.CreateModel(
            name='TerritoryInfoTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128, verbose_name='Заголовок')),
                ('text', yarky.helpers.HTMLField(verbose_name='Описание')),
                ('base', models.ForeignKey(editable=False, to='content.TerritoryInfo', related_name='translations')),
                ('language', models.ForeignKey(verbose_name='Language', to='translation.Language')),
            ],
            options={
                'verbose_name': 'Территория',
                'verbose_name_plural': 'Территории',
            },
        ),
        migrations.AlterModelOptions(
            name='slide',
            options={'verbose_name': 'Слайд', 'ordering': ['position'], 'verbose_name_plural': 'Слайды'},
        ),
        migrations.AlterModelOptions(
            name='slidetranslation',
            options={'verbose_name': 'Слайд', 'ordering': ['base__position'], 'verbose_name_plural': 'Слайды'},
        ),
        migrations.AlterField(
            model_name='slide',
            name='position',
            field=models.PositiveSmallIntegerField(verbose_name='Позиция', editable=False, default=0),
        ),
    ]
