# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import yarky.helpers
import translation.models


class Migration(migrations.Migration):

    dependencies = [
        ('translation', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Системное название', unique=True, max_length=150)),
                ('slug', models.SlugField(verbose_name='Код для URL', unique=True, max_length=150)),
                ('inline', models.BooleanField(default=False, verbose_name='Скрытая')),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Статьи',
                'verbose_name': 'Статья',
            },
            bases=(models.Model, translation.models.TranslationGetterMixin),
        ),
        migrations.CreateModel(
            name='PageTranslation',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Заголовок', max_length=150)),
                ('content', yarky.helpers.HTMLField(default='', verbose_name='Содержание', blank=True)),
                ('base', models.ForeignKey(editable=False, related_name='translations', to='content.Page')),
                ('language', models.ForeignKey(to='translation.Language', verbose_name='Language')),
            ],
            options={
                'verbose_name_plural': 'Статьи',
                'verbose_name': 'Статья',
            },
        ),
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Системное название', max_length=150)),
                ('image', models.ImageField(upload_to='slides/')),
                ('background', models.CharField(choices=[('darkorange', 'Оранжевый'), ('brightgreen', 'Зеленый'), ('acidpink', 'Ядреный розовый'), ('pink', 'Розовый')], max_length=32)),
                ('text_position', models.CharField(choices=[('Справа', 'right'), ('По центру', 'center'), ('Слева', 'left')], default='right', max_length=5)),
            ],
            bases=(translation.models.TranslationGetterMixin, models.Model),
        ),
        migrations.CreateModel(
            name='SlideGroup',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('key', models.SlugField(verbose_name='Ключ группы', max_length=32)),
            ],
            options={
                'verbose_name_plural': 'Группы слайдов',
                'verbose_name': 'Группа слайдов',
            },
        ),
        migrations.CreateModel(
            name='SlideTranslation',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Заголовок', max_length=128)),
                ('subtitle', models.CharField(verbose_name='Подзаголовок', max_length=128)),
                ('base', models.ForeignKey(editable=False, related_name='translations', to='content.Slide')),
                ('language', models.ForeignKey(to='translation.Language', verbose_name='Language')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='slide',
            name='group',
            field=models.ForeignKey(to='content.SlideGroup', related_name='slides'),
        ),
        migrations.AlterUniqueTogether(
            name='slidetranslation',
            unique_together=set([('base', 'language')]),
        ),
    ]
