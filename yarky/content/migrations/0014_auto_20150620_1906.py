# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import translation.models
import content.models
import yarky.helpers


class Migration(migrations.Migration):

    dependencies = [
        ('translation', '__first__'),
        ('content', '0013_auto_20150618_2119'),
    ]

    operations = [
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(verbose_name='Системное название', max_length=150, default='Номер', editable=False)),
                ('places', models.PositiveSmallIntegerField(verbose_name='Количество мест')),
                ('price', models.PositiveIntegerField(verbose_name='Цена за ночь')),
                ('image', models.ImageField(verbose_name='Фото', upload_to='rooms/')),
                ('published', models.BooleanField(verbose_name='Опубликовано', default=True)),
                ('position', models.PositiveSmallIntegerField(verbose_name='Позиция', default=0, editable=False)),
            ],
            options={
                'verbose_name': 'Номер',
                'verbose_name_plural': 'Номера',
                'ordering': ['position'],
            },
            bases=(models.Model, translation.models.TranslationGetterMixin),
        ),
        migrations.CreateModel(
            name='RoomTranslation',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(verbose_name='Заголовок', max_length=150)),
                ('short_title', models.CharField(verbose_name='Короткий заголовок', max_length=150)),
                ('description', yarky.helpers.HTMLField(verbose_name='Описание')),
                ('base', models.ForeignKey(to='content.Room', related_name='translations', editable=False)),
                ('language', models.ForeignKey(verbose_name='Language', to='translation.Language')),
            ],
            options={
                'verbose_name': 'Номер',
                'verbose_name_plural': 'Номера',
                'ordering': ['base__position'],
            },
        ),
        migrations.AlterField(
            model_name='slide',
            name='image',
            field=models.ImageField(verbose_name='Картинка', upload_to='slides/', validators=[content.models.ImageHeightValidator(height=565)]),
        ),
    ]
