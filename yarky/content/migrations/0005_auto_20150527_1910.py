# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0004_reservation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='breakfast',
            field=models.CharField(max_length=1, choices=[('1', 'Включен'), ('0', 'Не включен')]),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='room_type',
            field=models.CharField(max_length=1, choices=[('1', 'Многоместный на мансарде'), ('2', '4-х местный'), ('3', '2-х местный'), ('4', 'Улучшенный 2-х местный')]),
        ),
    ]
