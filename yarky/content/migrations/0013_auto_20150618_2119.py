# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import content.models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0012_auto_20150610_2346'),
    ]

    operations = [
        migrations.AddField(
            model_name='territoryinfo',
            name='published',
            field=models.BooleanField(verbose_name='Опубликовано', default=True),
        ),
        migrations.AlterField(
            model_name='slide',
            name='image',
            field=models.ImageField(upload_to='slides/', validators=[content.models.ImageHeightValidator(height=565)]),
        ),
    ]
