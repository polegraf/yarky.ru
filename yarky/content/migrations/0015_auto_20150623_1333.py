# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0014_auto_20150620_1906'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reservation',
            name='room_type',
        ),
        migrations.AddField(
            model_name='reservation',
            name='room',
            field=models.ForeignKey(to='content.Room', default=1),
            preserve_default=False,
        ),
    ]
