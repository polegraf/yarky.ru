# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import yarky.helpers


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0011_auto_20150610_2152'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='territoryinfo',
            options={'verbose_name': 'Территория', 'verbose_name_plural': 'Территории', 'ordering': ['territory', 'position']},
        ),
        migrations.AlterModelOptions(
            name='territoryinfotranslation',
            options={'verbose_name': 'Территория', 'verbose_name_plural': 'Территории', 'ordering': ['base__territory', 'base__position']},
        ),
        migrations.AlterField(
            model_name='slidetranslation',
            name='subtitle',
            field=yarky.helpers.HTMLField(blank=True, verbose_name='Подзаголовок', default=''),
        ),
    ]
