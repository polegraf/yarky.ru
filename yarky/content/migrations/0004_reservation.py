# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0003_auto_20150523_2140'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('date_in', models.DateField()),
                ('date_out', models.DateField()),
                ('guests', models.PositiveSmallIntegerField()),
                ('breakfast', models.IntegerField(choices=[('Включен', '1'), ('Не включен', '0')])),
                ('room_type', models.IntegerField(choices=[('Многоместный на мансарде', '1'), ('4-х местный', '2'), ('2-х местный', '3'), ('Улучшенный 2-х местный', '4')])),
                ('first_name', models.CharField(max_length=64)),
                ('last_name', models.CharField(max_length=64)),
                ('phone', models.CharField(max_length=64)),
                ('email', models.EmailField(max_length=254)),
                ('comment', models.TextField(null=True, blank=True)),
            ],
        ),
    ]
