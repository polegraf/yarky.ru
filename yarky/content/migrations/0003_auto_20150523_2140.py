# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0002_remove_slide_text_position'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slidetranslation',
            name='subtitle',
            field=models.CharField(max_length=128, default='', blank=True, verbose_name='Подзаголовок'),
        ),
        migrations.AlterField(
            model_name='slidetranslation',
            name='title',
            field=models.CharField(max_length=128, default='', blank=True, verbose_name='Заголовок'),
        ),
    ]
