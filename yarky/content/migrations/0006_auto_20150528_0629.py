# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0005_auto_20150527_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='breakfast',
            field=models.CharField(choices=[('0', 'Не включены'), ('1', 'Включены')], max_length=1),
        ),
    ]
