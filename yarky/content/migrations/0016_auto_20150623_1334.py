# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0015_auto_20150623_1333'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reservation',
            old_name='room',
            new_name='room_type',
        ),
    ]
