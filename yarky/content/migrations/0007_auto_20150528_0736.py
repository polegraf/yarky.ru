# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0006_auto_20150528_0629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slidetranslation',
            name='subtitle',
            field=models.TextField(verbose_name='Подзаголовок', blank=True, default=''),
        ),
    ]
