# coding: utf-8
from django import forms
from content.models import Reservation


class ReservationForm(forms.ModelForm):

    class Meta:
        model = Reservation
        fields = forms.ALL_FIELDS
