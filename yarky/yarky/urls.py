"""yarky URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
# from django.contrib import admin
# admin.autodiscover()
from django.conf.urls.static import static
from content.views import HomeView

urlpatterns = [

    url(r'^$', "translation.views.current_lang_redirect"),

    url(r'^(?P<lang>[a-z]{2})/$', HomeView.as_view(), name="home"),
    url(r'^reservation/$', "content.views.reservation", name="reservation"),
    url(r'^(?P<lang>[a-z]{2})/blog/', include("blog.urls", namespace='blog')),


    url(r'^admin/logout/$', 'admin.decorators.logout', name='admin_logout'),
    url(r'^admin/password_change/(?P<obj_id>\d+)/$',
        'admin.decorators.password_change', name='password_change'),
    url(r'admin/', include("admin.urls", namespace='admin')),
    # url(r'^admin/tags_list/$', tags_list, name='tags_list'),

]


if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', "django.contrib.staticfiles.views.serve"),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
