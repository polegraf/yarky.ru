# -*- coding: utf-8 -*-
from datetime import date
import json
import operator
from itertools import chain

from django import forms
from django.forms.util import flatatt
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils.encoding import force_str
from django.utils.safestring import mark_safe
from django.utils.html import escape, conditional_escape
from django.db import models
from django.core.serializers.json import DateTimeAwareJSONEncoder
from django.core.urlresolvers import reverse
from django.contrib.auth.models import Permission
from django.core.mail import send_mail

from variables import get_setting
from translation import translate as _, translator


def json_response(data, **httpresponse_kwargs):
    content = json.dumps(data)
    return HttpResponse(content,
                        content_type='application/json',
                        **httpresponse_kwargs)


class JSONResponseMixin(object):

    def render_to_response(self, context):
        "Returns a JSON response containing 'context' as payload"
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        "Construct an `HttpResponse` object."
        return HttpResponse(content,
                            content_type='application/json',
                            **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context)


class PossibleJSONResponseMixin(object):

    def render_to_json_response(self, context, **response_kwargs):
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(self.convert_context_to_json(context),
                            **response_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context, cls=ModelsEncoder)


class JSONResponseMixin(PossibleJSONResponseMixin):

    def render_to_response(self, context):
        return self.render_to_json_response(context)


class PrettySelectMultiple(forms.SelectMultiple):

    def __init__(self, *args, **kwargs):
        self.type = kwargs.pop("type", None)
        super(PrettySelectMultiple, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, choices=()):
        self._element_id = attrs['id']

        if value is None: value = []
        final_attrs = self.build_attrs(attrs, name=name)
        parent_output = [u'<div class="pretty-multiple sortable sortable-{0} droppable droppable-{0}" id="id_pretty_{0}" style="min-height: 150px; border: 1px solid #ccc;"{1}>'.format(name, flatatt(final_attrs))]

        options = self.render_options(choices, value, name)
        if options:
            parent_output.append(options)
        parent_output.append('</div>')

        output = u"""
            <div class="pretty-m2m">
                <a class="btn open" data-link="{0}" data-type="{1}"><i class="icon-search"></i></a>
                <div>
                    {2}
                </div>
            </div>
        """.format(reverse("admin:route-action", args=[self.type, name]), name, "\n".join(parent_output))

        return mark_safe(output)

    def render_option(self, selected_choices, option_value, option_label, name):
        option_value = force_str(option_value)
        return u"""<li data-id="{0}" class="draggable draggable-{2}">
            <input type="hidden" name="{2}" value="{0}">
            {1}
         </li>""".format(escape(option_value), conditional_escape(force_str(option_label)), name)

    def render_options(self, choices, selected_choices, name):
        # Normalize to strings.
        selected_choices = set([force_str(v) for v in selected_choices])

        output = []
        for option_value, option_label in chain(self.choices, choices):
            if not force_str(option_value) in selected_choices:
                continue
            if isinstance(option_label, (list, tuple)):
                output.append(u'<optgroup label="%s">' % escape(force_str(option_value)))
                for option in option_label:
                    option.append(name)
                    output.append(self.render_option(selected_choices, *option))
                output.append(u'</optgroup>')
            else:
                output.append(self.render_option(selected_choices, option_value, option_label, name))
        return u'\n'.join(output)


class PermPrettySelectMultiple(PrettySelectMultiple):

    def render_options(self, choices, selected_choices, name):
        permissions = Permission.objects.filter(id__in=selected_choices)
        perm_dict = dict(list(permissions.values_list('id',
                                                      'content_type__name')))

        selected_choices = set([force_str(v) for v in selected_choices])
        output = []
        for option_value, option_label in chain(self.choices, choices):
            if not force_str(option_value) in selected_choices:
                continue
            option_value = perm_dict[option_value]
            option_label = u"Управление %s" % option_value
            if isinstance(option_label, (list, tuple)):
                output.append(u'<optgroup label="%s">' % escape(
                    force_str(option_value)))
                for option in option_label:
                    option.append(name)
                    r_o = self.render_option(selected_choices, *option)
                    r_o not in output and output.append(r_o)
                output.append(u'</optgroup>')
            else:
                r_o = self.render_option(selected_choices, option_value,
                                         option_label, name)
                r_o not in output and output.append(r_o)
        return u'\n'.join(output)


class ModelsEncoder(DateTimeAwareJSONEncoder):

    def default(self, obj):
        if issubclass(obj.__class__, models.base.ModelBase):
            return object.__class__.__name__
        if issubclass(obj.__class__, models.Model):
            special = getattr(obj, "to_json", None)
            if special:
                return special() if callable(special) else special
            return obj.pk
        return super(ModelsEncoder, self).default(obj)


def get_current_season():
    return get_setting("current_season")

def get_next_season():
    if has_next_season():
        return get_setting("next_season")
    return None


def has_next_season():
    return bool(get_setting("next_season"))


def send_confirmation_code(subscribe):

    text = render_to_string("email/subscribe_code_{0}.html".format(translator.current_lang),
        context={
            "subscribe": subscribe,
        },
    )
    send_mail(_("Подтверждение подписки на новости Московской Государственной Филармонии"),
              text, settings.DEFAULT_FROM_EMAIL,
              [subscribe.email], fail_silently=False)


def build_cache_key(template, obj):
    return u"{0}.{1}.{2}.key".format(template, obj.pk, translator.current_lang)


class LazyLoadingMixin(object):

    paginate_by = 50

    def get_template_name(self):
        if self.request.is_ajax():
            return self.ajax_template_name
        return self.template_name

    def get_current_page(self):
        try:
            page = int(self.request.GET.get("page", 1))
        except (TypeError, ValueError):
            page = 1
        if page < 1:
            page = 1
        return page

    def paginate(self, qs):

        page = self.get_current_page()
        from_begin = self.request.GET.get('from_begin', "")

        if from_begin:
            return qs[:page*self.paginate_by] #if we will provide ?from_begin=true we will get whole queryset from entry 1 on page 1
        else:
            return qs[(page-1)*self.paginate_by:page*self.paginate_by]


def methodcache(name=None):

    def cache_decorator(func):
        if not name:
            cache_name = u"__{0}".format(func.__name__)
        else:
            cache_name = name

        def field_func(self, *args, **kwargs):
            if kwargs.get("force") or not hasattr(self, cache_name):
                setattr(self, cache_name, func(self, *args, **kwargs))
            return getattr(self, cache_name)

        return field_func

    return cache_decorator
