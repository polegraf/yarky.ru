# coding: utf-8
from django import forms
from django.db import models


class HTMLField(models.TextField):

    def formfield(self, **kwargs):
        defaults = {'widget': forms.Textarea(attrs={"class": "wysiwyg programe"})}
        defaults.update(kwargs)
        return super(HTMLField, self).formfield(**defaults)

    # def south_field_triple(self):
    #     "Returns a suitable description of this field for South."
    #     # We'll just introspect the _actual_ field.
    #     from south.modelsinspector import introspector
    #     field_class = "django.db.models.fields.TextField"
    #     args, kwargs = introspector(self)
    #     # That's our definition!
    #     return (field_class, args, kwargs)
