from suit.apps import DjangoSuitConfig


class CustomDjangoSuitConfig(DjangoSuitConfig):
    admin_name = 'Yarky Control Center'
    menu_position = 'vertical'
