# Create your views here.
from django.http import HttpResponse, Http404
from seo.models import ExternalStatsProof, SeoCore, RobotsTxt
from django.conf import settings

def robots_txt(request):
    try:
        robots = RobotsTxt.objects.latest("id")
    except:
        robots = RobotsTxt()
        
    response = HttpResponse(robots.robotstxt, mimetype="text/plain")
    return response