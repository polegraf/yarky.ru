# -*- coding: utf-8 -*-
from django.conf import settings
from django.utils.safestring import mark_safe

from chouwa.decorators import jinja2_global, jinja2_filter

from seo.models import SeoCore

@jinja2_global
def seo_var(lang, keyword):
    try:
        seo = SeoCore.objects.get(language__code=lang)
        return getattr(seo, keyword, "")
    except SeoCore.DoesNotExist:
        return u""
