# -*- coding: utf-8 -*-
"""
Models for Search Engine Optimization compatibility
"""
from django.db import models
from translation import lazy_translate as _


class SEOModel(models.Model):
    """
    Base abstract model. Add field for main meta-tags
        <title></title>
        <meta type="description">
        <meta type="keywords">
    """
    page_title = models.CharField(max_length=150, verbose_name=_(u"Суффикс заголовоков страниц"),
                                  blank=True, default="")
    page_description = models.CharField(max_length=150,
                                        verbose_name=_(u"Описание страницы"),
                                        blank=True, default="")
    page_keywords = models.CharField(max_length=150,
                                     verbose_name=_(u"Ключевые слова"),
                                     blank=True, default="")

    class Meta:
        abstract = True


class SeoCore(SEOModel):
    """
    Some usefull data for SEO. Not sure, that it should be a model-based.
    More of that - we should cache this data to process memory and refresh
    rarely or on data change
    """
    language = models.ForeignKey("translation.Language",
                                 verbose_name=_(u"Язык"), unique=True)

    index_title = models.CharField(max_length=255, verbose_name=_(u"Заголовок главной страницы"))

    title_separator = models.CharField(max_length=10,
                                    verbose_name=_(u"Разделитель заголовка"),
                                    default=u" :: ")

    class Meta:
        verbose_name = _("SEO core")
        verbose_name_plural = _("SEO core")

    def __str__(self):
        return _("SEO core")


class RobotsTxt(models.Model):
    """
    Very stupid model. Robots.txt should be a file, editable from admin.
    More of that - we need verify changes before save
    """
    robotstxt = models.TextField(verbose_name=u"robots.txt", blank=True)

    class Meta:
        verbose_name=u"robots.txt"
        verbose_name_plural=u"robots.txt"

    def __str__(self):
        return "robots.txt"
