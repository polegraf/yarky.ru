# -*- coding: utf-8 -*-
from django.conf import settings
from django.utils.safestring import mark_safe

from chouwa.decorators import jinja2_global, jinja2_filter
from translation import translator

from variables.models import Variable, Setting


@jinja2_global
def variable(title):
    try:
        return Variable.objects.get(
            title=title, language__code=translator.current_lang
        ).value
    except Variable.DoesNotExist:
        return u""

@jinja2_global
def setting(key):
    try:
        return Setting.objects.get(key=key).get_value()
    except Setting.DoesNotExist:
        return u""

