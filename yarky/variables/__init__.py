# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from variables import settings
from variables.models import Setting


class Configurator:

    _cache = None
    _cache_expire = None

    def get_setting(self, key):
        self._check_cache()  # Validate cache
        return self._cache[key].get_value()

    def _check_cache(self):
        """
        Validates cache. If there is no cache (first request after worker start)
        or cache is expired - rebuild it
        """
        if self._cache is None or self._cache_expire < datetime.now():
            self._rebuid_cache()

    def _rebuid_cache(self):
        """
        Loads settings from database to worker memory.
        """
        from .models import Setting
        self._cache = {}
        for setting in Setting.objects.all():
            self._cache[setting.key] = setting
        self._cache_expire = datetime.now()+timedelta(seconds=settings.CONFIGURATOR_CACHE_LIFETIME)

# Create singleton
configurator = Configurator()

del Configurator #remove Configurator class from namespace, to prevent new instances creation

def get_setting(key):
    return configurator.get_setting(key)
