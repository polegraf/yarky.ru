# -*- coding: utf-8 -*-
from django.conf import settings

CONFIGURATOR_CACHE_LIFETIME = getattr(settings, "CONFIGURATOR_CACHE_LIFETIME", 15)
