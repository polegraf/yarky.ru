# -*- coding: utf-8 -*-
import json
from django.db import models
from translation import lazy_translate as _

class Variable(models.Model):

    title = models.CharField(max_length=150, verbose_name=_(u"Title"))
    language = models.ForeignKey("translation.Language", verbose_name=_(u"Language"))
    value = models.TextField(verbose_name=_(u"Value"), blank=True)
    allow_html = models.BooleanField(default=False, verbose_name=_(u"Allow html tags"))

    class Meta:
        verbose_name = _(u"Переменная")
        verbose_name_plural = _(u"Переменные")
        unique_together = (("title", "language"),)

    def __str__(self):
        return self.title


class Setting(models.Model):

    var_types = (
        ("str", "Строка"),
        ("int", "Целое"),
        ("float", "С плавающей точкой"),
        ("date", "Дата"),
        ("json", "JSON"),
    )

    key = models.CharField(max_length=150, verbose_name=_(u"Ключ"), unique=True)
    value = models.TextField(verbose_name=_(u"Значение"), blank=True)
    var_type = models.CharField(max_length=25, verbose_name=_(u"Тип"),
                                choices=var_types)

    class Meta:
        verbose_name = _(u"Настройка")
        verbose_name_plural = _(u"Настройки")

    def __str__(self):
        return self.key

    def get_value(self):
        return getattr(self, "unpack_{0}".format(self.var_type), lambda x: x)(self.value)

    def set_value(self, value, var_type):
        self.value = getattr(self, "pack_{0}".format(var_type), lambda x: str(x))(value)

    def save(self, *args, **kwargs):
        self.set_value(self.value, self.var_type)
        return super(Setting, self).save(*args, **kwargs)

    def unpack_int(self, value):
        try:
            return int(value)
        except (ValueError, TypeError):
            return None

    def unpack_float(self, value):
        try:
            return float(value)
        except (ValueError, TypeError):
            return None

    def unpack_json(self, value):
        try:
            return json.loads(value)
        except (ValueError, TypeError):
            return None

    def pack_json(self, value):
        try:
            return json.dumps(value)
        except (ValueError, TypeError):
            return None
