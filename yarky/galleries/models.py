# -*- coding: utf-8 -*-
from django.db import models

from translation.models import TranslationModel, TranslationGetterMixin
from translation import lazy_translate as _


class Gallery(models.Model):

    title = models.CharField(max_length=150,
                             verbose_name=_(u"Название"))

    key = models.SlugField(max_length=32, verbose_name=_(u"Код для вставки"))

    class Meta:
        verbose_name = _(u"Галерея")
        verbose_name_plural = _(u"Галерея")

    def __str__(self):
        return self.title


class GalleryImage(models.Model, TranslationGetterMixin):

    gallery = models.ForeignKey(Gallery, verbose_name=u"Галерея", related_name="images")

    image = models.ImageField(upload_to="gallery/images", max_length=255,
                              verbose_name=_(u"Изображение"))

    position = models.IntegerField(default=0)

    class Meta:
        verbose_name = _(u"Изображение")
        verbose_name_plural = _(u"Изображения")
        ordering = ["position",]

    def __str__(self):
        return _(u"Изображения")

    def get_absolute_url(self):
        return '#'


class GalleryImageTranslation(TranslationModel):

    base_model = GalleryImage

    title = models.TextField(verbose_name=u"Заголовок", blank=True, null=True)

    class Meta:
        verbose_name = _(u"Изображение")
        verbose_name_plural = _(u"Изображения")
        ordering = ["base__position",]

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return self.base.get_absolute_url()
