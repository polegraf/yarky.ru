import re
from django.template.loader import render_to_string
from chouwa.decorators import jinja2_filter
from galleries.models import GalleryImageTranslation
from translation import translator

gallery_pattern = re.compile("(<|&lt;){2}GALLERY:(?P<key>[0-9a-z\-_]+)(>|&gt;){2}")


@jinja2_filter
def insert_gallery(text):

    for (_, key, __) in gallery_pattern.findall(text):
        images = GalleryImageTranslation.objects.filter(
            base__gallery__key=key,
            language__code=translator.current_lang
        )
        if images:
            gallery_code = render_to_string("galleries/inline.html",
                                            {"photos": images})
        else:
            gallery_code = ""

        text = re.sub("(<|&lt;){2}GALLERY\:%s(>|&gt;){2}" % key, gallery_code, text)

    return text
