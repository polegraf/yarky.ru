# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import translation.models


class Migration(migrations.Migration):

    dependencies = [
        ('translation', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Системное название', max_length=150)),
                ('key', models.SlugField(verbose_name='Код для вставки', max_length=32)),
            ],
            options={
                'verbose_name': 'Галерея',
                'verbose_name_plural': 'Галерея',
            },
        ),
        migrations.CreateModel(
            name='GalleryImage',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('image', models.ImageField(verbose_name='Изображение', upload_to='gallery/images', max_length=255)),
                ('gallery', models.ForeignKey(verbose_name='Галерея', to='galleries.Gallery')),
            ],
            options={
                'verbose_name': 'Изображение',
                'verbose_name_plural': 'Изображения',
            },
            bases=(models.Model, translation.models.TranslationGetterMixin),
        ),
        migrations.CreateModel(
            name='GalleryImageTranslation',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Заголовок', db_index=True, max_length=250)),
                ('base', models.ForeignKey(editable=False, related_name='translations', to='galleries.GalleryImage')),
                ('language', models.ForeignKey(verbose_name='Language', to='translation.Language')),
            ],
            options={
                'verbose_name': 'Изображение',
                'verbose_name_plural': 'Изображения',
            },
        ),
    ]
