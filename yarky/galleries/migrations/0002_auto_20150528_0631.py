# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('galleries', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='galleryimagetranslation',
            name='title',
            field=models.TextField(blank=True, null=True, verbose_name='Заголовок'),
        ),
    ]
