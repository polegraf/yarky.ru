# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('galleries', '0004_auto_20150812_1243'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='galleryimagetranslation',
            options={'verbose_name': 'Изображение', 'verbose_name_plural': 'Изображения', 'ordering': ['base__position']},
        ),
        migrations.AlterField(
            model_name='galleryimage',
            name='gallery',
            field=models.ForeignKey(related_name='images', verbose_name='Галерея', to='galleries.Gallery'),
        ),
    ]
