# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('galleries', '0003_auto_20150605_1311'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='galleryimage',
            options={'verbose_name_plural': 'Изображения', 'ordering': ['position'], 'verbose_name': 'Изображение'},
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='position',
            field=models.IntegerField(default=0),
        ),
    ]
