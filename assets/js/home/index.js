$(document).ready(function(){

    var $main_carousel= $('#main-carousel'),
        $header= $('header');

    // main carousel
    (function(){
        if ($main_carousel.length > 0) {
            var styles_arr = [];
            $('.carousel-item', $main_carousel).each(function(){
                styles_arr.push($(this).data());
            });

            $.fn.extend({
                setStyle: function(styles, currentSlide, nextSlide) {
                    return this.each(function() {
                        var styles_len = styles.length;
                        for (var i=0; i<styles_len; i++) {
                            var style = styles[i];
                            $(this).removeClass(style + '-' + styles_arr[currentSlide][style])
                                .addClass(style + '-' + styles_arr[nextSlide][style]);
                        }
                    });
                }
            });

            $main_carousel.on('init', function(event, slick, direction){
                $(".slick-dots li", $main_carousel).on("click", function() {
                    $main_carousel.slick("slickSetOption", "autoplay", false);
                });
            });

            $main_carousel.slick({
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 7000,
                arrows: false,
                pauseOnHover: false,
                pauseOnDotsHover: true,
                dots: true
            });
            $main_carousel.on('beforeChange', function(event, slick, currentSlide, nextSlide){
                $main_carousel.setStyle(['bgrcolor'], currentSlide, nextSlide);
                $header.setStyle(['bgrcolor', 'title_color', 'subtitle_color'], currentSlide, nextSlide);
            });

            $header.setStyle(['bgrcolor', 'title_color', 'subtitle_color'], 0, 0);
        }
    })(); // end main carousel

    $('.carousel-blog').slick({
        infinite: true,
        slidesToShow: 3,
        pauseOnHover: true,
        pauseOnDotsHover: true,
        slidesToScroll: 1
    });

    for (var i = 1; i<=2; i++) {
        init_datepicker(i);
    }

    $(".introduction-screen-3").on('click', '.close-reservation', function(e) {
        e.preventDefault();
        $(".introduction-screen-3").addClass("hidden");
        $(".introduction-screen-1").removeClass("hidden");
        $("form").each(function() {
            $(this).get(0).reset();
        });

        $('#main-carousel').slick("slickPlay");
    });

    $('.introduction-screen-1 .reservation-button').on('click', function(e) {
        e.preventDefault();
        var date_in = $(".introduction-screen-1 .date_in").datepicker('getDate');
        var date_out = $(".introduction-screen-1 .date_out").datepicker('getDate');
        if (date_in && date_out) {
            e.preventDefault();
            $(".introduction-screen-1").addClass("hidden");
            $(".introduction-screen-2").removeClass("hidden");
            $(".introduction-screen-2 .date_in").datepicker('setDate', date_in);
            $(".introduction-screen-2 .date_out").datepicker('setDate', date_out);
            $(".introduction-screen-2 .room_type").change();
            $("#main-carousel").slick("slickPause");
        } else {
            if (!date_in) {$(".introduction-screen-1 .date_in").addClass("error")}
            if (!date_out) {$(".introduction-screen-1 .date_out").addClass("error")}
        }
    });


    $('.introduction-screen-2 .reservation-button').on('click', function(e) {
        e.preventDefault();
        $(".reservation-step-1").addClass("hidden");
        $(".reservation-step-2").removeClass("hidden");
        $(this).addClass("hidden");
        $(".introduction-screen-2 .back-button").removeClass("hidden");
    });

    $(".introduction-screen-2 .back-button").on('click', function(e) {
        e.preventDefault();
        $(".reservation-step-1").removeClass("hidden");
        $(".reservation-step-2").addClass("hidden");
        $(this).addClass("hidden");

    });


    $(".introduction-screen-2 select.guests").on('change', function(e) {
        var guests = parseInt($(this).val());
        var $room_type = $(".introduction-screen-2 .room_type");
        $("option", $room_type).each(function() {
            var $option = $(this);
            var min = 0;//$option.data("guests-min");
            var max = $option.data("guests-max");
            if (guests < min || guests > max) {
                $option.addClass("hidden");
            } else {
                $option.removeClass("hidden");
            }
        });
        if ($("option:selected", $room_type).is(".hidden")) {
            $room_type.val($("option:not(.hidden):first", $room_type).val());
        }
        calculate_cost();
    });

    $(".introduction-screen-2 .breakfast").change(function() {
        calculate_cost();
    });
    $(".introduction-screen-2 .room_type").change(function() {
        var room_type = $(this).val();
        $(".selected-room .room").html($(".room-item-"+room_type+" .room").clone());
        calculate_cost();
    });

    $(".submit-reservation").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass("inprocess")) {
            return false;
        }
        var fields_map = ["date_in", "date_out", "guests", "breakfast",
            "room_type", "first_name", "last_name", "phone", "email"];
        var errors = false;
        var data = {};
        for (i in fields_map) {
            var field_name = fields_map[i];
            var $field = $(".introduction-screen-2 [name=" + field_name + "]");
            var value = $field.val();
            if (!value) {
                errors = true;
                $field.addClass("error");
            } else {
                $field.removeClass("error");
                data[field_name] = value;
            }
        }
        data["comment"] = $(".introduction-screen-2 [name=comment]").val();
        if (!errors) {
            $this.addClass("inprocess");
            $.ajax({
                type: "POST",
                url: "/reservation/",
                data: data,
                complete: function() {
                    $this.removeClass("inprocess");
                },
                success: function(resposne) {
                    if (resposne["success"]) {
                        $(".reservation-result").html(resposne["content"]);
                        $(".introduction-screen-2").addClass("hidden");
                        $(".introduction-screen-3").removeClass("hidden");
                    } else {
                        for (var key in resposne) {
                            console.log(key, resposne[key]);
                            var $field = $(".introduction-screen-2 [name=" + key + "]");
                            $field.addClass("error");
                        }
                    }
                },
                dataType: "json"
            });
        }
    });
});


function calculate_cost() {
    var price = parseInt($("select.room_type option:selected").data("price"));
    var guests = parseInt($("select.guests").val());
    var breakfast = parseInt($("select.breakfast").val());
    var date_in = $(".introduction-screen-2 .calendar.date_in").datepicker('getDate');
    var date_out = $(".introduction-screen-2 .calendar.date_out").datepicker('getDate');
    if (!date_in || !date_out) {return;}
    var days = Math.round(Math.abs((date_in.getTime() - date_out.getTime())/(24*60*60*1000)));

    var cost = price*guests*days;
    if (breakfast == 1) {
        cost = cost + 180*guests*days;
    }

    $(".introduction-screen-2 .cost").text(cost);
}

function init_datepicker(i) {
    var $base = $(".introduction-screen-"+i);
    var DATEPICKER_PARAMS = {language: CURRENT_LANG, autoclose: true, startDate: "01.12.2015"};
    var $datepicker_in = $("input.calendar.date_in", $base).datepicker(DATEPICKER_PARAMS);
    var $datepicker_out = $("input.calendar.date_out", $base).datepicker(DATEPICKER_PARAMS);

    $datepicker_in.on("changeDate", function(){
        var date_in = $datepicker_in.datepicker('getDate');
        var date_out = $datepicker_out.datepicker('getDate');
        var next_out = new Date(date_in.getTime() + 24*60*60*1000);
        if (date_in >= date_out || !date_out) {
            $datepicker_out.datepicker('setDate', next_out);
        }
        $datepicker_out.datepicker('setStartDate', next_out);
    });
    $datepicker_in.on("hide", function(){
        calculate_cost();
    });
    $datepicker_out.on("hide", function(){
        calculate_cost();
    });

    $('table tr td.old, table tr td.new', '.datepicker').text('');
}
