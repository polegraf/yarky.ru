$(document).ready(function(){

    var $territory_img = $(".territory-img");
    var $territory_map = $(".territory-map");
    var $territory_info = $(".territory-info");
    var $territory_area = $(".territory-area");
    var $places = $(".places");

    $('.places-list a').on('click', function(e) {
        if ($(this).parent().hasClass("active"))
            $(this).removeClass("active");
        else
            $('.places-list .active').removeClass('active');
            setTimeout(() => $(this).parent().toggleClass('active'), 500);
        e.preventDefault();
    });

    $territory_area.hover(function(e) {
        var color = $(this).data("color");
        $territory_map.addClass("hover "+color);
    }, function(e) {
        var color = $(this).data("color");
        $territory_map.removeClass("hover "+color);
    });
    $territory_area.click(function(e) {
        e.preventDefault();
        var color = $(this).data("color");
        $places.removeClass("red green blue yellow");
        $(".places-content").addClass("hidden");
        if ($(".places-content."+color).length == 0) {
            $(".places-close").click();
            return;
        }
        $territory_info.addClass("hidden");
        $places.removeClass("hidden");
        $places.addClass(color);
        $(".places-content."+color).removeClass("hidden");
        $territory_map.addClass("active");
    });
    $(".places-close").click(function(e) {
        e.preventDefault();
        $territory_info.removeClass("hidden");
        $places.removeClass("red green blue yellow");
        $(".places-content").addClass("hidden");
        $places.addClass("hidden");
        $territory_map.removeClass("active ");
    });
});
