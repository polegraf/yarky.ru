var placeMarks = [

    // Metro 1
    {
        "coords": [60.002534, 30.296565],
        "image": "/static/images/icons/map/metro.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "м. Пионерская"
    },

    // Metro 2
    {
        "coords": [59.999740, 30.366517],
        "image": "/static/images/icons/map/metro.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "м. Площадь Мужества"
    }, {
        "coords": [59.997534, 30.361708],
        "image": "/static/images/icons/map/trolley.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка троллейбуса №6"
    }, {
        "coords": [59.998674, 30.362010],
        "image": "/static/images/icons/map/taxi.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка маршрутки К-252 и К-50"
    },

    // Metro 3
    {
        "coords": [59.98535, 30.301017],
        "image": "/static/images/icons/map/metro.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "м. Чёрная Речка"
    },  {
        "coords": [59.987121, 30.302551],
        "image": "/static/images/icons/map/bus.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка автобуса №94 и маршрутки 346"
    }, {
        "coords": [59.987316, 30.302610],
        "image": "/static/images/icons/map/trolley.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка троллейбуса №6"
    }, {
        "coords": [59.986684, 30.303864],
        "image": "/static/images/icons/map/trolley.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка троллейбуса №34"
    }, {
        "coords": [59.986988, 30.304670],
        "image": "/static/images/icons/map/taxi.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка маршруток К-252 и К-298"
    }, {
        "coords": [59.985052, 30.301312],
        "image": "/static/images/icons/map/shatl.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Фирменный шатл yarky"
    }, {
        "coords": [59.987228, 30.305368],
        "image": "/static/images/icons/map/tram.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка трамваев №21 и №40"
    },

    // Metro 4
    {
        "coords": [59.984818, 30.344084],
        "image": "/static/images/icons/map/metro.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "м. Лесная"
    }, {
        "coords": [59.985130, 30.344739],
        "image": "/static/images/icons/map/taxi.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка маршрутки К-240а"
    }, {
        "coords": [59.985829, 30.342046],
        "image": "/static/images/icons/map/tram.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка трамвая №20"
    },
    // {
    //     "coords": [59.985552, 30.347704],
    //     "image": "/static/images/icons/map/shatl.png",
    //     "size": [40, 54], "offset": [-20, -54],
    //     "balloon": "Фирменный шатл yarky"
    // },

    // Yarky

    {
        "coords": [59.997611, 30.330827],
        "image": "/static/images/icons/map/yarky.png",
        "size": [100, 100], "offset": [-50, -50]
    },
    {
        "coords": [59.999610, 30.329375],
        "image": "/static/images/icons/map/tram.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка трамваев №20, №21 и №40"
    },
    {
        "coords": [59.998368, 30.329805],
        "image": "/static/images/icons/map/taxi.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка маршрутки К-240а, К-346 и К-50"
    },
    {
        "coords": [59.997800, 30.331822],
        "image": "/static/images/icons/map/bus.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка автобуса №94, троллейбуса №6 и №34"
    },
    {
        "coords": [59.997199, 30.331044],
        "image": "/static/images/icons/map/taxi.png",
        "size": [40, 54], "offset": [-20, -54],
        "balloon": "Остановка маршруток К-252 и К-298"
    }

];

$(document).ready(function() {
    if ($("#yarkyfest-map").length == 0) {
        return
    }
    ymaps.ready(function(){
        var map = new ymaps.Map("yarkyfest-map", {
            center: [59.997520, 30.331075],
            zoom: 14,
            controls: ["zoomControl"]
        });
        map.behaviors.disable("scrollZoom");

        $(placeMarks).each(function(index, mark) {
            var placemark = new ymaps.Placemark(mark.coords, {
                "hintContent": mark.balloon
            }, {
                iconLayout: 'default#image',
                iconImageHref: mark.image,
                iconImageSize: mark.size,
                iconImageOffset: mark.offset
            });
            map.geoObjects.add(placemark);
        });

        var HowToGetControllClass = function (options) {
            HowToGetControllClass.superclass.constructor.call(this, options);
            this._$content = null;
            this._geocoderDeferred = null;
        };

        ymaps.util.augment(HowToGetControllClass, ymaps.collection.Item, {

            onAddToMap: function (map) {
                HowToGetControllClass.superclass.onAddToMap.call(this, map);
                this._lastCenter = null;
                this.getParent().getChildElement(this).then(this._onGetChildElement, this);

                setTimeout(function() {
                    $(".way h3 a").on('click', function(e) {
                        e.preventDefault();
                        $(this).parent().toggleClass('active');
                    });
                    $(".way h3 a").trigger('click');
                }, 200);
            },

            onRemoveFromMap: function (oldMap) {
                this._lastCenter = null;
                if (this._$content) {
                    this._$content.remove();
                    this._mapEventGroup.removeAll();
                }
                HowToGetControllClass.superclass.onRemoveFromMap.call(this, oldMap);
            },

            _onGetChildElement: function (parentDomContainer) {
                this._$content = $('<div class="way"> <div class="way-content"><h3><a href="#" class="way-trigger">Как добраться</a></h3><div class="way-info">'+$("#how-to-get").html()+'</div></div></div>').appendTo(parentDomContainer);
            }

        });

        var howToGetControl = new HowToGetControllClass();

        map.controls.add(howToGetControl, {
            float: 'none',
            position: {
                top: 40,
                right: 10
            }
        });

    });

});
