$(document).ready(function() {
    $('.article-img-carousel').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true,
        pauseOnDotsHover: true
    });
    //$("#blog-calendar").datepicker({language: CURRENT_LANG});
    initBlogCalendar();
});

var initBlogCalendar = function () {
    $("#blog-calendar .prev-date, #blog-calendar .next-date").click(function(e) {
        e.preventDefault();
        var year = $(this).data("year");
        var month = $(this).data("month");

        $.getJSON("/"+CURRENT_LANG+"/blog/get_calendar/", {"year": year, "month": month}, function(json) {
            $("#blog-calendar").replaceWith(json.calendar);
            initBlogCalendar();
        });
    });
}
