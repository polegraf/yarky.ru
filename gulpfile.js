var gulp = require('gulp');

var rigger = require('gulp-rigger');

var postcss = require('gulp-postcss'),
    fs = require("fs"),
    postcssImport = require("postcss-import"),
    postcssMixins = require('postcss-mixins'),
    postcssVars = require("postcss-simple-vars"),
    postcssNested = require("postcss-nested"),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('autoprefixer-core'),
    mqpacker = require('css-mqpacker'),
    csswring = require('csswring');

var evilIcons = require("gulp-evil-icons");

var imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

var uglify = require('gulp-uglify'),
    babel = require("gulp-babel"),
    concat = require('gulp-concat');


var lr = require('tiny-lr'),
    server = lr();

var rimraf = require('rimraf');

var browserSync = require('browser-sync'),
    reload = browserSync.reload;

var path = {
    build: { // куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        style: 'build/styles/',
        img: 'build/images/',
        fonts: 'build/fonts/'
    },
    assets: {
        html: 'assets/**/*.html',
        js: 'assets/js/**/*.js',
        style: 'assets/styles/**/*.css',
        img: 'assets/images/**/*.*',
        fonts: 'assets/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9001,
    logPrefix: "Yarky"
};

gulp.task('compile:html', function () {
    return gulp.src(path.assets.html)
        .pipe(rigger())
        .pipe(evilIcons())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('compile:css', function () {
    var processors = [
        postcssImport(),
        postcssMixins,
        postcssNested,
        postcssVars,
        autoprefixer({browsers: ['last 2 version']}),
        mqpacker,
        csswring
    ];
    return gulp.src('assets/styles/*.css')
      .pipe(sourcemaps.init())
      .pipe(postcss(processors))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(path.build.style))
      .on('error', console.log)
      .pipe(reload({stream: true}));
});

gulp.task('compile:js', function() {
    return gulp.src(path.assets.js)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe( babel({ loose: 'all' }) ).on('error', console.log)
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('compile:images', function() {
    return gulp.src(path.assets.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('compile:fonts', function() {
    return gulp.src(path.assets.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}));
});

gulp.task('compile', ['compile:html', 'compile:css', 'compile:js', 'compile:images', 'compile:fonts']);


// Локальный сервер для разработки
gulp.task('server', function() {
    browserSync(config);
});

// Запуск сервера разработки gulp watch
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('compile');

    gulp.watch(path.assets.html, function() {
        gulp.run('compile:html');
    });
    gulp.watch(path.assets.style, function() {
        gulp.run('compile:css');
    });
    gulp.watch(path.assets.js, function() {
        gulp.run('compile:js');
    });
    gulp.watch(path.assets.img, function() {
        gulp.run('compile:images');
    });
    gulp.watch(path.assets.fonts, function() {
        gulp.run('compile:fonts');
    });

    gulp.run('server');
});

gulp.task('just_watch', function() {
    // Предварительная сборка проекта
    gulp.run('compile');

    gulp.watch(path.assets.html, function() {
        gulp.run('compile:html');
    });
    gulp.watch(path.assets.style, function() {
        gulp.run('compile:css');
    });
    gulp.watch(path.assets.js, function() {
        gulp.run('compile:js');
    });
    gulp.watch(path.assets.img, function() {
        gulp.run('compile:images');
    });
    gulp.watch(path.assets.fonts, function() {
        gulp.run('compile:fonts');
    });
});


gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['compile', 'server', 'watch'])



// for django
gulp.task('django_watch', function() {
    gulp.watch(path.assets.style, ['compile:css']);
    gulp.watch(path.assets.js, ['compile:js']);
    gulp.watch(path.assets.img, ['compile:images']);
});

gulp.task('django', ['compile', 'django_watch'], function() {
    browserSync({
        open: false,
        port: 3001,
        proxy: "localhost:8080"
    });
});
