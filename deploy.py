import os
import sys
from contextlib import contextmanager
from paramiko import SSHClient
import paramiko
import time

DEFAULT_DEPLOY_USER = 'yarky'
DEFAULT_DEPLOY_HOST = 'yarky.itcanfly.org'


def env(name, default=None):
    return os.environ.get(name, default)


def ensureconnect(func):
    def wrapper(self, *args, **kwargs):
        if not self.client:
            self.connect()
        return func(self, *args, **kwargs)
    return wrapper

class SimpleSSHDeployer(object):

    def __init__(self, user=None, host=None, key=None, verbose=True):
        self.client = None
        self.user = user or env("DEPLOY_USER", DEFAULT_DEPLOY_USER)
        self.host = host or env("DEPLOY_HOST", DEFAULT_DEPLOY_HOST)
        self.key = key or env("DEPLOY_KEY")
        self.verbose = verbose
        self.pwd = None

    def connect(self):
        self.client = SSHClient()
        self.client.load_system_host_keys()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(hostname=self.host, username=self.user)

    @contextmanager
    @ensureconnect
    def cd(self, path):
        try:
            self.pwd = path
            yield path
        finally:
            self.pwd = None

    @ensureconnect
    def run(self, cmd):
        if self.pwd:
            command = "cd {}; {}".format(self.pwd, cmd)
        else:
            command = cmd
        shell = self.client.invoke_shell()
        try_n = 0

        while not shell.recv_ready():
            time.sleep(1)
            try_n += 1
            if try_n >= 10:
                print("Connection timeout")
                return

        header = shell.recv(1024)

        shell.send(command+'\n')
        shell.recv(len(command+'\n')).decode("utf-8")

        while not shell.recv_ready():
            time.sleep(1)
            try_n += 1
            if try_n >= 10:
                print("Connection timeout")
                return

        out = shell.recv(4096).decode("utf-8")

        print(out.split("\r\n"))
        results = []
        for line in out.split("\r\n"):
            if not line.strip():
                continue

            if self.verbose:
                sys.stdout.write("[{}@{}]: {} -> {}\n".format(self.user, self.host, cmd, line))
            results.append(line.strip())
        return u"\n".join(results)


if __name__ == "__main__":
    client = SimpleSSHDeployer()
    # client.run("echo $PATH")
    with client.cd("yarky.ru/yarky"):
        client.run("git pull")
        # client.run("npm install")
        # client.run("gulp compile")
        # client.run("python manage.py collectstatic --noinput")
        # client.run("python manage.py migrate")
        # client.run("pkill -f uwsgi")
